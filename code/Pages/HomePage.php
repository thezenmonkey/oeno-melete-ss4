<?php
	
class HomePage extends Page {
	
	/**
	 * Static vars
	 * ----------------------------------*/
	
	

	/**
	 * Object vars
	 * ----------------------------------*/
	
	
	
	/**
	 * Static methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Data model
	 * ----------------------------------*/

	private static $db = array (
		
	);
	

	private static $has_one = array (
		
	);
	
	private static $has_many = array (
		
	);
	
	/**
	 * Common methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Accessor methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Controller actions	
	 * ----------------------------------*/
	
	
	
	/**
	 * Template accessors
	 * ----------------------------------*/
	
	public function LatestWorks($limit = 6) {
		
		$artworks = Artwork::get()->filter(array('Quantity:GreaterThan' => 0))->sort("MPDateAdded", "DESC")->limit($limit);
		
		return $artworks->count() ? $artworks : false;
		
	}
	
	public function LatestArtistWorks($limit = 6 ){
		
		$artworkList = new ArrayList();
		
		$artworks = Artwork::get()->filter(array('Quantity:GreaterThan' => 0))->sort("MPDateAdded", "DESC");
		
		$artistIDs = array();
		
		$i = 1;
		
		foreach ($artworks as $artwork) {
			if($i <= $limit) {
				if(!in_array($artwork->Parent()->ID, $artistIDs)) {
					$artworkList->push($artwork);
					$artistIDs[] = $artwork->Parent()->ID;
					$i++;
				}
			} else {
				break;
			}
		}
		
		return $artworkList->count() ? $artworkList : false;
	}
	
	public function LatestNews($count = 3) {
		$news = BlogPost::get();
		
		return $news->count() ? $news->limit($count) : false;
	}
	
	/**
	 * Object methods
	 * ----------------------------------*/

	function requireDefaultRecords() {
		if(!SiteTree::get_by_link("home")){
			$homepage = new HomePage();
			$$homepage->Title = "Home";
			$homepage->URLSegment = "home";
			$homepage->Sort = 1;
			$homepage->write();
			$homepage->publish('Stage', 'Live');
			$homepage->flushCache();
			DB::alteration_message('Home Page created', 'created');
		} else {
			$homepage = SiteTree::get_by_link("home");
			if($homepage->ClassName != "HomePage") {
				$homepage = $homepage->newClassInstance("HomePage");
				$homepage->write();
				$homepage->publish('Stage', 'Live');
				$homepage->flushCache();
				DB::alteration_message('Home changed to HomePage', 'changed');
			}
		}
	
		parent::requireDefaultRecords();
	}

	
}


class HomePage_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
		"SearchArt", "doArtSearch"
	);

	public function init() {
		parent::init();
		
	}
	
}