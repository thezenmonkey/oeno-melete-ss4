<?php

class Artwork extends DataObject {
	
	/**
	 * Static vars
	 * ----------------------------------*/
	
	

	/**
	 * Object vars
	 * ----------------------------------*/
	
	
	
	/**
	 * Static methods
	 * ----------------------------------*/
	
	public function canView($member = null) {
        return Permission::check('CMS_ACCESS_ArtworkAdmin', 'any', $member);
    }


    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_CMSMain', 'any', $member);
    }
	
	/**
	 * Data model
	 * ----------------------------------*/

	private static $db = array (
		"Title" => "Varchar",
		"URLSegment" => "Varchar",
		"Content" => "HTMLText",
		
		
		
		"Price" => "Currency",
		"Year" => "Int",
		"Medium" => "Varchar(255)",
		"Subject" => "Varchar(255)",
		"OnDisplay" => "Boolean",
		"Width" => "Decimal",
		"Height" => "Decimal",
		"Depth" => "Decimal",
		"Units" => "Enum(array('Metric','Imperial'))",
		"Dimensions" => "Enum(array('2D','3D'))",
		"EditionType" => "Enum(array('Open','Unique', 'Limited'))",
		"EditionSize" => "Int",
		"Quantity" => "Int",
		"FramePrice" => "Currency",
		"FrameWidth" => "Decimal",
		"FrameHeight" => "Decimal",
		
		"Sort" => "Int",
        "UseLocalContent" => "Boolean"
		
/*
		"TempContent" => "HTMLText",
		"TempURL" => "Varchar",
		"TempTitle" => "Varchar(255)",
		"TempCreated" => "SS_DateTime",
		"TempLastEdited" => "SS_DateTime"
*/
	);
	

	private static $has_one = array (
		"Category" => "ArtworkCategory",
		"Artist" => "Artist",
		"Locations" => "Location",
	);
	
	private static $has_many = array (
		"Editions" => "ArtworkEdition"
	);
	
	private static $many_many = array (
		"Images" => "Image",
		//"Tags" => "ArtworkTag"
	);
	
	private static $belongs_many_many = array (
		"Exhibits" => "Exhibit",
		//"Videos" => "Video"
	);
	
	
	private static $summary_fields = array(
		'Thumbnail',
		'Title',
		'ArtistName',
		'Price'
	);
	
/*
	private static $searchable_fields = array(
		'Title',
		'Content',
		//'Tag',
		'Price',
		'Height',
		'Width'
	);
*/
	
	private static $create_table_options = array(
        'MySQLDatabase' => 'ENGINE=MyISAM'
    );
    
    private static $better_buttons_actions = array (
        'MPReSync',
        'MPReSyncImages'
    );
	
	
	/**
	 * Common methods
	 * ----------------------------------*/
	public function getCMSFields() {
		$fields = parent::getCMSFIelds();
		//$pages = SiteTree::get()->filter(array('ClassName' => "Artist"));
		//$parentID = $this->ParentID ? : $pages->first()->ID;
		
		
/*
		$sizeField = CompositeField::create(array(
			NumericField::create("Width"),
			NumericField::create("Height"),
			NumericField::create("Depth"),
			DropdownField::create("Units", "Units", singleton('Artwork')->dbObject('Units')->enumValues())
		));
		
*/
		
		$fields->removeFieldsFromTab("Root.Main", array(
			"Width",
			"Height",
			"Depth",
			"Quantity",
			"EditionSize"
		));
		
		$sizeField = SelectionGroup::create(
			"Dimensions",
			array(
				new SelectionGroup_Item(
					"2D",
					CompositeField::create(array(
						NumericField::create("Width"),
						NumericField::create("Height")
					)),
					"2D"
				),
				new SelectionGroup_Item(
					"3D",
					CompositeField::create(array(
						NumericField::create("Width2", "Width", $this->Width),
						NumericField::create("Height2", "Height", $this->Height),
						NumericField::create("Depth")
					)),
					"3D"
				)
			)
		);
		
		$edtionField = SelectionGroup::create(
			"EditionType",
			array(
				new SelectionGroup_Item(
					"Open",
					null,
					"Open"
				),
				new SelectionGroup_Item(
					"Unique",
					null,
					"Unique"
				),
				new SelectionGroup_Item(
					"Limited",
					CompositeField::create(array(
						NumericField::create("EditionSize"),
						NumericField::create("Quantity")
					)),
					"Limited"
				)
			)
		);
		
		
		$fields->removeFieldsFromTab("Root.Main", array("URLSegment","MenuTitle"));
	
		//$fields->renameField("Title", "Title");
		//$fields->renameField("Content", "Description");
		$fields->renameField("ParentID", "Artist");
		
		$fields->addFieldsToTab("Root.Main", HTMLEditorField::create("Content", "Description")->setRows(4));
		
		$fields->insertAfter(CurrencyField::create("Price"), "ParentID");
		$fields->insertAfter(TextField::create("Year"), "Title");
		
		$fields->addFieldsToTab("Root.Main", array(
			//DropdownField::create('ParentID', _t('CatalogManager.PARENTPAGE', 'Artist'), $pages->map('ID', 'Title'), $parentID),
			TextField::create("Medium"),
			TextField::create("Subject"),
			
			//SIZE
			$sizeField,
			DropdownField::create("Units", "Units", singleton('Artwork')->dbObject('Units')->enumValues()),
			
			//EDITION
			$edtionField,
			
			//FRAME
			NumericField::create("FramePrice"),
			NumericField::create("FrameWidth"),
			NumericField::create("FrameHeight"),
			
			//Qnty
			ReadonlyField::create("Quantity")
			
		));
		
		//$fields->changeFieldOrder("Title","ParentID","Content","Metadata");
		
		$fields->insertAfter(CheckboxField::create("OnDisplay", "Currently On Display"), "Subject");

        $fields->insertBefore(CheckboxField::create("UseLocalContent", "Use Local Content")->setDescription('Override Masterpeice Description'),"Content");
		
		
		//$fields->addFieldsToTab("Root.Gallery", GalleryUploadField::create("Images"));
		
		
		return $fields;
	}
	
	public function getBetterButtonsUtils() {
        $fields = parent::getBetterButtonsUtils();
        
        if($this->MPArtistID != 0) {
            $fields->push(
            	BetterButtonCustomAction::create('MPReSync', 'Re-sync')
            		->setRedirectType(BetterButtonCustomAction::REFRESH)
					->setSuccessMessage('Synced Please Re-Publish')
            
            );
            
             $fields->push(
            	BetterButtonCustomAction::create('MPReSyncImages', 'Get Images')
            		->setRedirectType(BetterButtonCustomAction::REFRESH)
					->setSuccessMessage('Images Downloaded Re-Publish')
            
            );
        }
        
        return $fields;
    }
	
	public function MPReSync() {
		set_time_limit ( 30 );
		$records = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND title.iTitleNum = ".$this->MPTitleID);
		
		if($records) {
			
			foreach($records as $record) {
				//Debug::show($change);
				if ( !empty($record->Error) )
					throw new Exception($record->Error);
				
				if( !$record->TitleId || $record->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
				
				$changes = MasterpieceUtils::ArtworkBridge($this, $record);
			}
			
			
			$this->write();
			//echo "Written Artwork<br>";
			//$this->publish('Stage', 'Live');
			$this->flushCache();
			
		}
		
		return true;
	}
	
	Public function MPReSyncImages() {
		$imageCount = $this->MPImages;
		
		if($this->Images()) {
			
			foreach($this->Images() as $image) {
				$image->delete();
				
			}
			
			$i = 1;
			while($i <= $imageCount) {
				//set_increase_time_limit_max();
				$image = null;
				
				$image = MasterpieceUtils::GetArtworkImage($this, $i);
				
				if($image) {
					$this->Images()->add($image);
				}
				
			}
			
			$this->write();
			//echo "Written Artwork<br>";
			//$this->publish('Stage', 'Live');
			$this->flushCache();
			
		}
		
		return true;
		
	}
	
	
	
	public function onBeforeWrite() {
		
		
		if($this->Dimensions == "3D") {
			$this->Width == $this->Width2;
			$this->Height == $this->Height2;
		}

		
		if($this->Artist()->NoPrice == 1) {
			$this->NoPrice == 1;
		}
		
		if(!$this->URLSegment && $this->Title){
			$filter = new URLSegmentFilter();
			$this->URLSegment = $filter->filter( $this->Title );
			
			$existingArt = $this->Artist()->Artworks()->filter(array("URLSegment" => $this->URLSegment))->first();
			
			if($existingArt) {
				$count = 2;
				while($exisitngArtwork = $this->Artist()->Artworks()->filter(array("URLSegment" => $this->URLSegment.'-'.$count))->first()) {
					$count++;
				}
				
				$this->URLSegment =  $this->URLSegment.'-'.$count;
			}
			
		}
		
		parent::onBeforeWrite();
	}
	
	public function onBeforeDelete() {
		if($this->Images()->count()) {
			
			foreach ($this->Images() as $image) {
				$image->delete();
			}
			
		}
		
		parent::onBeforeDelete();
		
	}
	
	/**
	 * Accessor methods
	 * ----------------------------------*/
	
	public function getThumbnail() {
		if( $this->OrderedImages()->exists() && $this->OrderedImages()->count() )
            return $this->OrderedImages()->First()->SetWidth(100);
	}
	
	public function getArtistName() {
		if($this->Artist()->exists()) return $this->Artist()->Title;
	}
	
	public function getNoPrice() {
		return $this->Artist()->NoPrice;
	}
	
	public function getLastName() {
		return $this->Artist()->Contact()->LastName;
	}
	
	public function getTitleAndArtist() {
		return $this->Title.' by '.$this->Artist()->Title;
	}
	
	/**
	 * Controller actions	
	 * ----------------------------------*/
	
	
	
	/**
	 * Template accessors
	 * ----------------------------------*/
	
	public function getFormattedPrice() {
		if($this->Price > 0) {
			return "$".floor($this->Price);
		}

        return false;
	}
	
	public function getImperialSize() {
		if ($this->Units == "Imperial") {
			$size = $this->PrettyNumber($this->Height).
					' &times; '.
					$this->PrettyNumber($this->Width).
					(
						($this->Dimensions == "3D") ? 
						' &times; '.
						$this->PrettyNumber($this->Depth) : ''
					);
		} else {
			$size = $this->PrettyNumber($this->Height*0.393701).
					' &times; '.
					$this->PrettyNumber($this->Width*0.393701).
					(
						($this->Dimensions == "3D") ? 
						' &times; '.
						$this->PrettyNumber($this->Depth*0.393701) : ''
					);
		}
		
		return $size;
	}
	
	public function getMetricSize() {
		if ($this->Units == "Metric") {
			$size = $this->PrettyNumber($this->Height).
					' &times; '.
					$this->PrettyNumber($this->Width).
					(
						($this->Dimensions == "3D") ? 
						' &times; '.
						$this->PrettyNumber($this->Depth) : ''
					);
		} else {
			$size = $this->PrettyNumber($this->Height*2.54).
					' &times; '.					
					$this->PrettyNumber($this->Width*2.54).
					(
						($this->Dimensions == "3D") ? 
						' &times; '.
						$this->PrettyNumber($this->Depth*2.54) : ''
					);
		}
		
		return $size;
	}
	
	public function PrettyNumber($number) {
		return round($number, 1);
	}
	/**
	 * Object methods
	 * ----------------------------------*/

	public function getCustomSearchContext() {
        $fields = $this->scaffoldSearchFields(array(
            'restrictFields' => array(
            	'Title',
            	'Content',
				//'Tag',
				'Price',
				'Height',
				'Width'
            ),
            'fieldClasses' => array(
	            'Title' => "TextField",
            	'Content' => "TextField",
				//'Tag',
				'Price' => "RangeField",
				'Height' => "TextField",
				'Width' => "TextField"
            )
        ));
        $filters = array(
            'Title' => new PartialMatchFilter('Title'),
        	'Content' => new PartialMatchFilter('Content'),
			//'Tag',
			'Price' => new WithinRangeFilter('Price'),
			'Height' => new WithinRangeFilter('Height'),
			'Width' => new WithinRangeFilter('Width'),
        );
        return new SearchContext(
            $this->class, 
            $fields, 
            $filters
        );
		
    }
    
    
    
    
    public function Purchase() {
        return Controller::curr()->Purchase();
	}
    
    public function HoldForm() {
        return Controller::curr()->HoldForm();
	}
	
	public function InfoForm() {
        return Controller::curr()->InfoForm();
	}
    
    public function EmailThis(){
        return Controller::curr()->EmailThis();
		
    }
	
	
	//redundatnt
	public function doPurchase($data, $form, $request) {
		

		
		if( !array_key_exists('Confirm', $data) ) {
			return $this->controller->redirectBack();
		}
		
		$artwork = Artwork::get()->byID($data['ArtworkID']);
		
		if(!$artwork) {
			return $this->controller->redirectBack();
		}
		
		$clientEmail = new Email();
		$galleryEmail = new Email();
		
		
		$clientEmail->setFrom('info@oenogallery.com')
				    ->setTo($data['Email'])
				    ->setSubject('Purchase info for '.$artwork->Title.' by '.$artwork->Parent()->Title)
				    ->setTemplate('CustomerEmailPurchase')
				    ->populateTemplate(new ArrayData(array(
				        'Client' => $data,
				        'Artwork' => $artwork
				    )));
				    
		$galleryEmail->setFrom('purchase@oenogallery.com')
				    ->setTo('info@oenogallery.com')
				    ->setSubject('Purchase request for '.$artwork->Title.' by '.$artwork->Parent()->Title)
				    ->setTemplate('GalleryEMailPurchase')
				    ->populateTemplate(new ArrayData(array(
				        'Client' => $data,
				        'Artwork' => $artwork
				    )));
		
		Debug::show($clientEmail);
		
		Debug::show($galleryEmail);
		
		
	}
	
	
	
	public function Link() {
		if($this->Quantity > 0 && $this->Artist()->isPublished()) {
			
			return $this->Artist()->Parent()->URLSegment.'/'.$this->Artist()->URLSegment.'/art/'.$this->URLSegment;
		} else {
			return false;
		}
	}
	
	public function AbsoluteLink() {
		return Director::absoluteBaseURL().$this->Link();
	}
	
	public function PrevNextPage($Mode = 'next') {
		
		if($Mode == 'next'){
			$Where = array("ArtistID" => $this->ArtistID, "Quantity:GreaterThan" => 0, "ID:GreaterThan" => $this->ID);
			$Sort = "ASC";
		} elseif($Mode == 'prev'){
			$Where = array("ArtistID" => $this->ArtistID, "Quantity:GreaterThan" => 0, "ID:LessThan" => $this->ID);
			$Sort = "DESC";
		} else{
			return false;
		}
		
		return Artwork::get()->filter($Where)->sort("ID",$Sort)->First();
	
	}
	
	public function CheckExperiment() {
		if($this->ClassName == "Artwork") {
			return true;
		} else {
			return false;
		}
	}
	
	public function ExperimentVariation() {
		
		$cookie = Cookie::get("oenogoogleexpvar");
		
		if(!isset($cookie)) {
			$variation = rand(0,3);
			Cookie::set("oenogoogleexpvar", $variation, 1);
			$cookie = $variation;
		}
		return $cookie;
	}

	
}



class Artwork_Images extends DataObject {

    static $db = array (
        'PageID' => 'Int',
        'ArtworkID' => 'Int',
        'ImageID' => 'Int',
        'Caption' => 'Text',
        'SortOrder' => 'Int'
    );
    
    
}
