<?php


class ArtworkHolder extends Page {
	
	/**
	 * Static vars
	 * ----------------------------------*/
	
	

	/**
	 * Object vars
	 * ----------------------------------*/
	
	
	
	/**
	 * Static methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Data model
	 * ----------------------------------*/

	private static $db = array (
		
	);
	

	private static $has_one = array (
		
	);
	
	private static $has_many = array (
		
	);
	
	/**
	 * Common methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Accessor methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Controller actions	
	 * ----------------------------------*/
	
	
	
	/**
	 * Template accessors
	 * ----------------------------------*/
	
	public function Artwork() {
		 $artwork = Artwork::get()->filter(array("Quantity:GreaterThan" => 0))->sort(array("MPDateAdded" => "DESC"));
		 
		 if($artwork->count()) {
			 return $artwork;
		 } else {
			 return false;
		 }
		 
	}
	
	
	/**
	 * Object methods
	 * ----------------------------------*/

	

	
}


class ArtworkHolder_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
		"PaginatedArtwork", "FullSearchArt", "SearchArt", "doArtSearch", "doReset"
	);

	public function init() {
		parent::init();

	}
	
	public function PaginatedArtwork() {
		$artwork = Artwork::get()->filter(array("Quantity:GreaterThan" => 0, "Historical" => 0))->sort(array("MPDateAdded" => "DESC"));
		
		 if($artwork->count()) {
			 return new PaginatedList($artwork, $this->request);
		 } else {
			 return false;
		 }
	}

    public function ArtSearch() {
        return ($this->getAction() == "SearchArt") ? true : false;
    }

    public function SearchKeywords() {

        return $this->request->requestVar('Keywords') ? $this->request->requestVar('Keywords') : false;
    }

	public function FullSearchArt() {
        $context = singleton('Artwork')->getCustomSearchContext();

        $priceBreaks = array(
	        "*" => "Any Price",
	        "2500" => "Less than $2500",
	        "5000" => "$2,501-5,000",
	        "10000" => "$5,000-10,000",
	        "20000" => "$10,000-20,000",
	        "50000" => "$20,000-50,000",
	        "50001" => "More than $50,000"
        );

        if ($this->ClassName == "ArtworkCategoryPage") {
            $categoryField = HiddenField::create("Category", "Category", $this->CategoryID);
        } else {
            $categoryField = DropdownField::create(
                "Category",
                "Category",
                ArtworkCategory::get()->exclude('ID', 10)->map('ID', 'Title')
            )->setEmptyString('(Select Category)');
        }

        $fields = FieldList::create(
	        
	        TextField::create("Keywords", '')->setAttribute('placeholder', 'Artist Name or Keyword (comma separated)')->addExtraClass('has-placeholder')->setRightTitle("Artist Name or Keyword (comma separated)"),
	        
	        LiteralField::create('AdvancedTrigger', "<div class='filter-trigger-holder'><a id='FilterTrigger' href='#' class='advanced-filter-trigger button tiny secondary'>Advanced Filters<span class='arrow-down'></span></a></div>"),
	        CompositeField::create(
                $categoryField,
				DropdownField::create("PriceRange", "Price Range", $priceBreaks),
				CompositeField::create(
			        LiteralField::create("WidthLabel","<label class='composite-label'>Width</label>"),
			        TextField::create("MinWidth")->setAttribute('placeholder', 'Min Width')->setAttribute("min", Artwork::get()->min("Width")),
			        TextField::create("MaxWidth")->setAttribute('placeholder', 'Max Width')->setAttribute("max", Artwork::get()->max("Width"))
		        ),
		        CompositeField::create(
			       LiteralField::create("HeightLabel","<label id='HeightLabel' class='composite-label'>Height</label>"),
			       TextField::create("MinHeight")->setAttribute('placeholder', 'Min Height')->setAttribute("min", Artwork::get()->min("Height")),
			       TextField::create("MaxHeight")->setAttribute('placeholder', 'Max Height')->setAttribute("max", Artwork::get()->max("Height"))
		        )
	        )->addExtraClass('advanced-filters')->removeExtraClass('nolabel'),
	        
	        //CheckboxField::create("Historical","Search Only Historical Works"),
	        FormAction::create('doArtSearch', 'Search Artwork')->addExtraClass("button primary"),
	        FormAction::create('doReset', 'Reset Filters')->addExtraClass("button secondary")
        );
        
        
        
        $form = Form::create(
        	$this,
        	"SearchArt",
            $fields,
            new FieldList()
        );

        //Load data from postVars or getVars depending on if the form is submitted from an external page
        if($this->request->postVars()) {
            //Form Submitted from Own age
            return $form->loadDataFrom($this->request->postVars())->disableSecurityToken();
        } else {
            //Form subitted from another page
            return $form->loadDataFrom(Controller::curr()->getRequest()->getVars())->disableSecurityToken();
        }
    }
    
    
    public function doReset(){
		
		$redirect = SiteTree::get_by_link("find-art");
		$this->redirect($redirect->Link());
		return;
    }
    
    public function doArtSearch($data, $form, $request) {
	    
	    $filterArray = array();
	    $wordFilter = array();
	    $artistFilter = array();
	    //build filter
		
		
		if(array_key_exists('Keywords', $data) && $data['Keywords']) {
		    if( stristr($data['Keywords'], ',') ) {
			    
			    //$cleanWords = str_replace(' ,', ',', str_replace(', ', ',', $data['Keywords'])) ;
			    $cleanWords = array_map('strtolower', explode(',', $data['Keywords'])  )  ;
			    
			    foreach($cleanWords as $cleanword) {
				    $cleanword = $cleanword.' ';
			    }
			    
			    $wordFilter["Title:PartialMatch"] = $cleanWords;
				$wordFilter["Content:PartialMatch"] = $cleanWords;
				
				$wordFilter["MPCustom1:PartialMatch"] = $cleanWords;
				$wordFilter["MPCustom2:PartialMatch"] = $cleanWords;
				$wordFilter["MPCustom3:PartialMatch"] = $cleanWords;
				
				$artistFilter["Parent.Title:ExactMatch"] = $cleanWords;
		    } else {
			    
			    if(strtolower($data['Keywords'])  == 'red') {
				    $data['Keywords'] = 'red ';
			    }
			    
			    $wordFilter["Title:PartialMatch"] = $data['Keywords'];
				$wordFilter["Content:PartialMatch"] = $data['Keywords'];
				
				$wordFilter["MPCustom1:PartialMatch"] = $data['Keywords'];
				$wordFilter["MPCustom2:PartialMatch"] = $data['Keywords'];
				$wordFilter["MPCustom3:PartialMatch"] = $data['Keywords'];
				
				$artistFilter["Parent.Title:PartialMatch"] = $data['Keywords'];
		    }
		    
	    }
	    
	    
	    if( array_key_exists('Category', $data) && $data['Category'] ) {
		    $filterArray['CategoryID'] = $data['Category'];
	    }
	    
	    if (array_key_exists('PriceRange', $data)) {
	    
		    switch ($data['PriceRange']) {
		    	case "2500":
		    		$filterArray["Price:LessThanOrEqual"] = 2500;
		    		break;
		    	case "5000":
		    		$filterArray["Price:GreaterThanOrEqual"] = 2500;
					$filterArray["Price:LessThanOrEqual"] = 5000;
					break;
				case "10000":
		    		$filterArray["Price:GreaterThanOrEqual"] = 5000;
					$filterArray["Price:LessThanOrEqual"] = 10000;
					break;
				case "20000":
		    		$filterArray["Price:GreaterThanOrEqual"] = 10000;
					$filterArray["Price:LessThanOrEqual"] = 20000;
					break;
				case "50000":
		    		$filterArray["Price:GreaterThanOrEqual"] = 20000;
					$filterArray["Price:LessThanOrEqual"] = 50000;
					break;
				case "50001":
		    		$filterArray["Price:GreaterThanOrEqual"] = 50000;
		    		break;
		    }
	    }
	    
		
		if(array_key_exists('PriceRange', $data) && $data['PriceRange'] && $data['PriceRange'] != "*" && $data['PriceRange'] != "50001") {
			$filterArray['NoPrice'] = 0;
		}
		
		if(array_key_exists('Historical', $data) && $data['Historical']) {
		    $filterArray['Historical'] = 1;
		}
	    
	    if (array_key_exists('MinWidth', $data) || array_key_exists('MaxWidth', $data)) {
		    if($data['MinWidth'] && $data['MaxWidth']) {
			    $filterArray["Width:GreaterThanOrEqual"] = $data['MinWidth'];
			    $filterArray["Width:LessThanOrEqual"] = $data['MaxWidth'];
		    } elseif($data['MinWidth']) {
			    $filterArray["Width:GreaterThanOrEqual"] = $data['MinWidth'];
		    } elseif($data['MaxWidth']) {
			    $filterArray["Width:LessThanOrEqual"] = $data['MaxWidth'];
		    }
	    }
	    
	    if (array_key_exists('MinHeight', $data) || array_key_exists('MaxHeight', $data)) {
		    if($data['MinHeight'] && $data['MaxHeight']) {
			    $filterArray["Height:GreaterThanOrEqual"] = $data['MinHeight'];
			    $filterArray["Height:LessThanOrEqual"] = $data['MaxHeight'];
		    } elseif($data['MinHeight']) {
			    $filterArray["Height:GreaterThanOrEqual"] = $data['MinHeight'];
		    } elseif($data['MaxHeight']) {
			    $filterArray["Height:LessThanOrEqual"] = $data['MaxHeight'];
		    }
	    }
		$filterArray["Quantity:GreaterThan"] = 0;
		
		$artists = false;
		
		// Find if Any Keywords ar ean artist Name
		if(array_key_exists('Keywords', $data) && $data['Keywords']) {
			$artists = Artist::get()->filter($artistFilter);
		}
		
		
		if($artists && $artists->count()) {

            if ($artists->count() == 1) {
                $this->redirect($artists->First()->Link());
            }

			$results = new ArrayList();
			$newWordFilter = null;
			// Only Search those Artists
			foreach ($artists as $artist) {
				$childFilterArray = $filterArray;
				$childFilterArray['ArtistID'] = $artist->ID;
				
				
				if( stristr($data['Keywords'], ',') ) {
					
					$newKeywords = explode(',',trim(str_replace(strtolower($artist->title), '', strtolower($data['Keywords']))));
					
					$searchTerms;
					foreach($newKeywords as $word) {
						if(trim($word) != false) {
							$searchTerms[] = trim($word);
						}
					}
					
					$newWordFilter["Title:PartialMatch"] = $searchTerms;
					$newWordFilter["Content:PartialMatch"] = $searchTerms;
				
					$newWordFilter["MPCustom1:PartialMatch"] = $searchTerms;
					$newWordFilter["MPCustom2:PartialMatch"] = $searchTerms;
					$newWordFilter["MPCustom3:PartialMatch"] = $searchTerms;
				}
				
				if(!is_null($newWordFilter)) {
					$works = Artwork::get()->filterAny($newWordFilter)->filter($childFilterArray);
				} else {
					$works = Artwork::get()->filter($childFilterArray);
				}
				
				
				$results->merge($works);
				$results->unshift($artist);
				//$results->merge(  );
			}
			
			
			
		} else {
			 $results = Artwork::get()->filter($filterArray)->filterAny($wordFilter);
		}
       
        
       
        
        if($results->count()) {
	        return $this->customise(array(
	            'ArtResults' => new PaginatedList($results, $this->request),
	            'Request' => http_build_query($this->request->postVars())
	        ));
        } else {
	        return $this->customise(array(
	            'NoArt' => true
	        ));
        }
        
    }
    
	
}