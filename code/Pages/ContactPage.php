<?php
	
class ContactPage extends Page {
	
	/**
	 * Static vars
	 * ----------------------------------*/
	
	

	/**
	 * Object vars
	 * ----------------------------------*/
	
	
	
	/**
	 * Static methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Data model
	 * ----------------------------------*/

	private static $db = array (
		
	);
	

	private static $has_one = array (
		
	);
	
	private static $has_many = array (
		
	);
	
	/**
	 * Common methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Accessor methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Controller actions	
	 * ----------------------------------*/
	
	
	
	/**
	 * Template accessors
	 * ----------------------------------*/
	
	
	
	/**
	 * Object methods
	 * ----------------------------------*/

	function requireDefaultRecords() {
		if(!SiteTree::get_by_link("contact-us")){
			$page = new ContactPage();
			$page->Title = "Contact Us";
			$page->URLSegment = "contact-us";
			$page->Sort = 1;
			$page->write();
			$page->publish('Stage', 'Live');
			$page->flushCache();
			DB::alteration_message('Contact Page created', 'created');
		} else {
			$page = SiteTree::get_by_link("contact-us");
			if($page->ClassName != "ContactPage") {
				$page = $page->newClassInstance("ContactPage");
				$page->write();
				$page->publish('Stage', 'Live');
				$page->flushCache();
				DB::alteration_message('Contact Us changed to ContactPage', 'changed');
			}
		}
	
		parent::requireDefaultRecords();
	}

	
}


class ContactPage_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
	);

	public function init() {
		parent::init();
		
	}
	
}