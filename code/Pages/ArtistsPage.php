<?php
	
class ArtistsPage extends Page {
	
	/**
	 * Static vars
	 * ----------------------------------*/
	
	

	/**
	 * Object vars
	 * ----------------------------------*/
	
	
	
	/**
	 * Static methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Data model
	 * ----------------------------------*/

	private static $db = array (
		
	);
	

	private static $has_one = array (
		
	);
	
	private static $has_many = array (
		
	);
	
	/**
	 * Common methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Accessor methods
	 * ----------------------------------*/
	
	
	
	/**
	 * Controller actions	
	 * ----------------------------------*/
	
	
	
	/**
	 * Template accessors
	 * ----------------------------------*/
	

	
	/**
	 * Object methods
	 * ----------------------------------*/

	function requireDefaultRecords() {
		if(!SiteTree::get()->filter(array("ClassName" => "ArtistsPage"))->First()){
			$page = new ArtistsPage();
			$page->Title = "Artists";
			$page->URLSegment = "artists";
			$page->Sort = 1;
			$page->write();
			$page->publish('Stage', 'Live');
			$page->flushCache();
			DB::alteration_message('Artist Page created', 'created');
		}
	
		parent::requireDefaultRecords();
	}

	
}


class ArtistsPage_Controller extends Page_Controller {
	
	private static $allowed_actions = array (
		"Artists", "RepresentedArtists", "OtherArtists", "PaginationCache", "RepCache"
	);

	public function init() {
		parent::init();
		
/*
		if(Director::is_ajax()) {
			$this->isAjax = true;
		}
		else {
			$this->isAjax = false;
		}
*/
		
	}
	
/*
	public function index() {
		if (Director::is_ajax()) {
		    return $this->renderWith(array("AjaxArtists"));
		} else {
			return Array();
		}
	}
*/
	
	public function Artists() {
		$artists = Artist::get();
		
		return $artists;
	}
	
	public function RepresentedArtists() {
		
		$artists = $this->Children()->filter(array("IsRepresented" => 1));
		
		return $artists->count() ? $artists->sort("LastName", "ASC") : false;
	}
	
	public function OtherArtists() {
		
		
		$artistList = new ArrayList();
		$artists = $this->Children()->filter(array("IsRepresented" => 0, "Historical" => 0));
		
		
		foreach ($artists as $artist) {
			if($artist->Artworks()->exclude("Quantity", 0)->count() != 0) {
				$artistList->push($artist);
			}
		}
		
		return new PaginatedList($artistList->sort("LastName", "ASC"), $this->request);
	}
	
	public function RepCache() {
		
		$reqestVars = Controller::getRequest()->getVars();
		
		if(array_key_exists('start', $reqestVars)) {
			$val = 1;
		} else {
			$val = 0;
		}
		
		return implode('_', array(
			'repart',
			$this->ID,
	        $val,
	        Artist::get()->max('LastEdited')
	    ));
	}
	
	public function PaginationCache() {
		
		$reqestVars = Controller::getRequest()->getVars();
		
		if($reqestVars['start']) {
			$start = $reqestVars['start'];
		} else {
			$start = 0;
		}
		
		return implode('_', array(
	        'artpage',
	        $start,
			$this->ID,
	        $reqestVars['ajax'],
	        Artist::get()->max('LastEdited')
	    ));
	}
	
}