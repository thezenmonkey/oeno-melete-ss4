<?php

class MasterpieceUtils extends Controller {

	public static function TagProcessor($item) {
		
		if(!$item) {
			return false;
		}

        return true;
	}
	
	public static function grab_image($url,$saveto){
		$ch = curl_init ($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		$raw=curl_exec($ch);
		curl_close ($ch);
		if(file_exists($saveto)){
		    unlink($saveto);
		}
		$fp = fopen($saveto,'x');
		fwrite($fp, $raw);
		fclose($fp);
	}
	
	
	public static function getMasterpeiceItem($class = null, $filter = "") {
		
		$companyId = "1A95-CCGH-6E59";
			    
	    $service = new MasterpieceService("http://".$_SERVER['HTTP_HOST'].Director::BaseURL().MELETE_DIR."/code/ThirdParty/masterpiece.wsdl");
	    
	    if($class == 'Artist') {
			
			$items = new ArtistRecordArray();
			$items = $service->GetArtistRecords($companyId, $filter);
			
	    } elseif ($class == 'Artwork') {
		    
		    $items = new TitleRecordArray();
			$items = $service->GetTitleRecords($companyId, $filter);
			echo "artwork<br>\n";
			
			//Debug::show($items);
		    
	    } elseif ($class == 'Event') {
		    
		    $items = new EventRecordArray();
			$items = $service->GetEventRecords($companyId, $filter);
		    
	    } elseif ($class == 'Category') {
		    
		    $catmed = new CategoryMediumRecordArray();
			$catmed = $service->GetCategoryMediumRecords($companyId, '');
			
			//Debug::show($catmed);
			
			return $catmed;
		    
	    } else {
		    echo "No Valid Class";
		    return false;
	    }
	    
	    if ( !empty($items->Error) ) {
		    throw new Exception($items->Error);
		    echo $items->Error."<br>\n";
		    return false;
	    }
		
		return $items;
	}
	
	public static function ArtworkBridge($artworkPage, $mpRecord) {
		

		
		$artworkPage->MPTitleID	  	= $mpRecord->TitleId;
		$artworkPage->MPArtistID  	= $mpRecord->ArtistId;
		$artworkPage->MPScanCode  	= $mpRecord->ScanCode;
		$artworkPage->MPArtCode	  	= $mpRecord->ArtCode;
		$artworkPage->MPTitleText1	= $mpRecord->TitleText1;
		$artworkPage->MPTitleText2	= $mpRecord->TitleText2;
		$artworkPage->MPTitleText3	= $mpRecord->TitleText3;
		$artworkPage->MPCustom1	  	= $mpRecord->Custom1;
		$artworkPage->MPCustom2	  	= $mpRecord->Custom2;
		$artworkPage->MPCustom3	  	= $mpRecord->Custom3;
		$artworkPage->MPDateAdded 	= $mpRecord->DateAdded;
		$artworkPage->MPDateUpdate	= $mpRecord->DateUpdated;
		$artworkPage->MPImages		= $mpRecord->Images;
		$artworkPage->MPDescription	= $mpRecord->Description;
		
		($mpRecord->DimUnits == "E") ? $artworkPage->Units = "Imperial" : $artworkPage->Dimensions = "Metric";
		($mpRecord->DimType == 3) ? $artworkPage->Dimensions = "3D" : $artworkPage->Dimensions = "2D";
		
		$artworkPage->Title 		= $mpRecord->Title;
		$artworkPage->Price		 	= $mpRecord->Price;
		$artworkPage->Year		 	= $mpRecord->Circa;
		$artworkPage->Medium	 	= $mpRecord->Medium;
		$artworkPage->Subject	 	= $mpRecord->Subject;
		$artworkPage->OnDisplay	 	= 1;
		$artworkPage->Width		 	= $mpRecord->Width;
		$artworkPage->Height	 	= $mpRecord->Height;
		$artworkPage->Depth		 	= $mpRecord->Depth;
		$artworkPage->EditionType	= $mpRecord->EditionType;
		$artworkPage->EditionSize	= $mpRecord->EditionSize;
		$artworkPage->Quantity	 	= $mpRecord->Quantity;
		$artworkPage->FramePrice 	= $mpRecord->FramePrice;
		$artworkPage->FrameWidth 	= $mpRecord->FrameHeight;
		$artworkPage->FrameHeight	= $mpRecord->FrameWidth;
		
		if($mpRecord->Category) {
			$cat = MasterpieceUtils::FindOrMakeCategory($mpRecord->Category);
			
			if($cat) {
				$artworkPage->CategoryID = $cat->ID;
			}
		}
		
		if(strlen($mpRecord->Description) > 0) {
			$outdoor = stripos(' '.$mpRecord->Description, "@outdoor@");
			$secondary = stripos(' '.$mpRecord->Description, "@secondary@");
			
			echo "Secondary is ".$secondary."<br>\n";
			echo "Outdoor is ".$outdoor."<br>\n";
			
			if($secondary === false) {
				$artworkPage->Secondary = 0;
			} else {
				$artworkPage->Secondary = 1;
				echo "secondary<br>\n";
			}
			
			if($outdoor === false) {
				$artworkPage->Outdoor = 0;
			} else {
				$artworkPage->Outdoor = 1;
				echo "Outdoor<br>\n";
			}

            if($artworkPage->UselLocalContent == 0) {

                $newContent = str_ireplace("@outdoor@", '', str_ireplace("@secondary@", '', $mpRecord->Description));
                if( strlen($newContent) > 0 ) {
                    $artworkPage->Content = "<p>". $newContent ."</p>";
                } else {
                    $artworkPage->Content = '';
                }
            }

			
		}
		
		$artist = Artist::get()->filter(array("MPArtistID" => $artworkPage->MPArtistID ))->first();
		
		if( $artist && $artist->NoPrice == 1 ) {
			$artworkPage->NoPrice = 1;
		}
		
		if(strlen($artworkPage->MPTitleText1) > 0) {
			(stripos(' '.$artworkPage->MPTitleText1.' ', "@historical@")) ? $artworkPage->Historical = 1 : $artworkPage->Historical = 0;
			(stripos(' '.$artworkPage->MPTitleText1.' ', "@new@")) ? $artworkPage->IsLatest = 1 : $artworkPage->IsLatest = 0;
		}
		
		if(strlen($artworkPage->MPTitleText3) > 0) {
			if (stripos(' '.$artworkPage->MPTitleText3.' ', "@outdoor@")) {
				$artworkPage->Outdoor = 1;
			}
			
			if (stripos(' '.$artworkPage->MPTitleText3.' ', "@secondary@")) {
				$artworkPage->Secondary = 1;
			}
		}
		return $artworkPage;
	}
	
	public static function ArtistBridge($artistPage, $mpRecord) {
		
		
		$contact = MasterpieceUtils::findOrCreateContact($mpRecord);
		
		$artistPage->MPArtistID = $mpRecord->ArtistId;
		$artistPage->MPDateAdded = $mpRecord->DateAdded;
		$artistPage->MPDateUpdated = $mpRecord->DateUpdated;
		$artistPage->MPHasImage = $mpRecord->HasImage;
		$artistPage->MPHasBio = $mpRecord->HasBio;
		$artistPage->MPDescription = $mpRecord->Description;
		$artistPage->MPFirstTitle = $mpRecord->FirstTitleId;
		
		$artistPage->ContactID = $contact->ID;
		
		$mpRecord->Born ? $artistPage->BirthDate = $mpRecord->Born : false;
		$mpRecord->Died ? $artistPage->DeathDate = $mpRecord->Died : false;
		
	}
	
	
	public static function ExhibitBridge($exhibitPage, $mpRecord){
		$exhibitPage->Title 		= $mpRecord->EventTitle;
		$exhibitPage->StartDate 	= $mpRecord->StartDate;
		$exhibitPage->EndDate 		= $mpRecord->EndDate;
		$exhibitPage->Content 		= "<p>".$mpRecord->Description."</p>";
		$exhibitPage->ParentID 		= $exhibitHolder->ID;
		
		$exhibitPage->MPDateAdded 	= $mpRecord->DateAdded;
		$exhibitPage->MPDateUpdated	= $mpRecord->DateUpdated;
		$exhibitPage->MPEventID		= $mpRecord->EventId;
		$exhibitPage->MPDescription	= $mpRecord->Description;
		$exhibitPage->MPLocation 	= $mpRecord->Location;
		$exhibitPage->MPFeatured 	= $mpRecord->Featured;
		$exhibitPage->MPEventTite 	= $mpRecord->EventTitle;
					
	}
	
	public static function FindOrMakeCategory($cat) {
		$category = ArtworkCategory::get()->filter(array("Title" => $cat))->first();
		
		if(!$category) {
			$category = ArtworkCategory::create();
			
			$category->Title = $cat;
			
			$category->write();
		} 
		
		return $category;
	}
	
	public static function findOrCreateContact($mpRecord) {
		
		//FIND OR CREATE CONTACT
			if($mpRecord->LastName == '') {
				echo "No LastName<br>";
				$contact = Contact::get()->where("`FirstName` = '".$mpRecord->FirstName."' and `LastName` IS NULL")->first();
			} elseif ($mpRecord->FirstName == '') {
				echo "No FirstName<br>";
				$contact = Contact::get()->where("`FirstName` IS NULL AND `LastName` = '".$mpRecord->LastName."'")->first();
			} else {
				$contact = Contact::get()->filter(array("FirstName" => $mpRecord->FirstName, "LastName" => $mpRecord->LastName))->first();
			}
			
			
			if(!$contact) {
				echo "Create New Contact<br>";
				$contact = new Contact();
				
				$contact->FirstName = $mpRecord->FirstName;
				$contact->LastName = $mpRecord->LastName;
				
				$contact->write();
				
				
			}
			
			return $contact;
				
		
	}
	
	public static function findOrCreateArtwork($mpRecord) {
		
	}
	
	
	public static function findOrCreateArtist($artistID) {
		
		$artist = Artist::get()->filter(array("MPArtistID" => $artistID))->first();
		
		if(!$artist) {
			$news = MasterpieceUtils::getMasterpeiceItem('Artist', ' AND iArtist = '.$artistID);
			
			if($news) {
			
				foreach($news as $new) {
					if ( !empty($new->Error) ) {
						echo "error";
						throw new Exception($new->Error);
					}
					
					if( (!$new->FirstName && !$new->LastName) || $new->LastName == "--" ) {
						echo "No New";
						continue;
					}
					
					$artist = MasterpieceUtils::createArtist($new);
					
						
				}
			}
					
		}
		
		return $artist;
		
	}
	
	
	public static function CreateArtworkFromID($artworkID, $artist = null) {
		
		$items = MasterpieceUtils::getMasterpeiceItem('Artwork', ' AND title.iTitleNum = '.$artworkID);
		
		if($items) {
			foreach($items as $item) {
				if ( !empty($item->Error) ) {
					echo "error";
					throw new Exception($item->Error);
				}
				
				if( !$item->TitleId || $item->TitleId == "0" ) {
					echo "No New";
					continue;
				}
				
				if(!$artist) {
					$artist = Artist::get()->filter(array("MPArtistID" => $item->ArtistId))->first();
					
					if(!$artist) {
						return false;
					}
					
				}
				
				
				$artwork = MasterpieceUtils::createArtwork($item, $artist);
				
				$artwork->write();
				echo "Publish Artwork<br>";
				//$artwork->publish('Stage', 'Live');
				//$artwork->flushCache();
				
				return $artwork;
					
			}
		}

        return false;
		
	}
	
	
	public static function GetArtistPhoto($artist) {
		$companyId = "1A95-CCGH-6E59";
		$base = Director::baseFolder();
		set_time_limit ( 60 );
		$url = "http://masterpiecesolutions.org/common/imgbio.php?galleryId=".$companyId."&artistId=".$artist->MPArtistID."&new_width=1200&new_height=1200";
		
		//echo $url."<br>";
		
		$saveto = "Artists/".$artist->URLSegment;
		 
		$folder = Folder::find_or_make($saveto);
		$filename = $artist->URLSegment.'-picture.jpg';
		//echo '"'.$filename.'"<br>';
		
		MasterpieceUtils::grab_image($url,$filename);
		
		if(file_exists($filename)) {
			//echo " image written<br>";
			if (copy($filename, "$base/assets/$saveto/$filename")) {
				//echo " image copied<br>";
				unlink($filename);
				//echo " image deleted<br>";
				
				$image = Image::get()->filter(array("Filename" => "assets/$saveto/$filename"))->first();
				
				if(!$image){
	                $image = new Image();
	                $image->Filename = "assets/$saveto/$filename";
	                $image->Title = $artist->Contact()->FirstName.' '.$artist->Contact()->LastName.' Photo';
	                
	                $image->ParentID = $folder->ID;
	                //echo "Create Image<br>";
	                $image->write();
	                
                } else {
	                //echo "Image Exists<br>";
                }
                return $image;
				
			}
		}
		
		return false;
		
	}
	
	
	public static function createArtist($new) {
		$artistHolder = SiteTree::get_by_link("artists");
		
		if($artistHolder) {
			echo "Artist Page is ".$artistHolder->ID."<br>";
		}
		
		
		$artist = Artist::create();
					
		MasterpieceUtils::ArtistBridge($artist, $new);
		
		$artist->ParentID = $artistHolder->ID;
		
		$artist->write();
		echo $artist->Title." New Artist<br>";
		$artist->publish('Stage', 'Live');
		$artist->flushCache();
		
		if($artist->MPFirstTitle != 0) {
			//CREATe ARTWORK
			$artwork = Artwork::get()->filter(array("MPTitleID" => $artist->MPFirstTitle))->first();
			
			if(!$artwork) {
				
				$artwork = MasterpieceUtils::CreateArtworkFromID($artist->MPFirstTitle, $artist);
				
			}
			
		}
		//CHECK IF ARTIST HAS IMAGE
		if($artist->MPHasImage == 1) {
			
			$image = MasterpieceUtils::GetArtistPhoto($artist);
			
			if($image) {
				$artist->PictureID = $image->ID;
			}
			
		}
		
		//TODO REFRACTOR THIS SECTION
		
		if( stripos($artist->MPDescription,"@no-price@") !== false ) {
			$artist->NoPrice = 1;
		}
		
		
		//find video tag
		if( stripos($artist->MPDescription,"@video-start@") !== false ) {
			echo $artist->Title."<br>";
			$pattern = "/@video-start@(.*?)@video-end@/";
			preg_match_all($pattern, $artist->MPDescription, $matches);
			
			$i = 1;
			foreach ($matches[1] as $videoURL) {
				$video = Video::get()->filter(array("URL" => $videoURL, "ArtistID" => $artist->ID))->first();
				
				if(!$video) {
					$video = new Video();
					$video->Title = $artist->Title." ".$i;
					$video->URL = $videoURL;
					$video->ArtistID = $artist->ID;
					
					$video->write();
				}
				
				$i++;
			}
			
		}
		
		//find secondary tag
		if( stripos($artist->MPDescription,"@secondary@") !== false ) {
			$artist->Secondary = 1;
		}
		
		//find secondary tag
		if( stripos($artist->MPDescription,"@outdoor@") !== false ) {
			$artist->Outdoor = 1;
		}
		
		
		$artist->write();
		echo "Written Artist<br>";
		$artist->publish('Stage', 'Live');
		$artist->flushCache();
		
		return $artist;
	}
	
	public static function createArtwork($new, $artist) {
		
		$artwork = Artwork::create();
		
		MasterpieceUtils::ArtworkBridge($artwork, $new);
		
		$artwork->ArtistID = $artist->ID;
		
		$artwork->write();
		echo "Written Artwork Draft<br>";
		//$artwork->publish('Stage', 'Live');
		//$artwork->flushCache();
		
		if($artwork->MPImages > 0) {
			echo "Artwork has Image<br>";
			
			$imageCount = $artwork->MPImages;
			
			$i = 1;
			
			while ($i <= $imageCount) {
				
				$image = MasterpieceUtils::GetArtworkImage($artwork, $i);
				
				if($image) {
					echo "Image ".$i." downloaded<br>";
					$artwork->Images()->add($image);
					$image->destroy();
				}
				
				$i++;
				
			}
		}
		
		return $artwork;
	}
	
	public static function createExhibit($new) {
		$exhibitHolder = SiteTree::get_by_link("exhibitions");
		
		if(!$exhibitHolder) {
			return false;
		}
		
		$exhibit = Exhibit::create();
					
		MasterpieceUtils::ExhibitBridge($exhibit, $new);
		$exhibit->ParentID = $exhibitHolder->ID;
		$exhibit->write();
		echo "Written Exhibittion<br>";
		$exhibit->publish('Stage', 'Live');
		
		//Find Art
		$artworks = Artwork::get()->filter(array("MPTitleText3:PartialMatch" => $new->Featured));
		
		if($artworks->count()) {
			foreach ($artworks as $art) {
				$exhibit->Artworks()->add($art);
				echo  $art->Title." Added<br>";
			}
		}
		
		$exhibit->write();
		echo "Written Exhibittion<br>";
		$exhibit->publish('Stage', 'Live');
		$exhibit->flushCache();
		
		return $exhibit;
		
	}
	
	public static function GetArtworkImage($artwork, $i) {
        set_time_limit ( 60 );
		$companyId = "1A95-CCGH-6E59";
		$base = Director::baseFolder();
		//set_increase_time_limit_max('60');
		
		$saveto = "Artists/".$artwork->Artist()->URLSegment;
						
		$folder = Folder::find_or_make($saveto);
		
		$url = "http://masterpiecesolutions.org/common/imgpiece.php?galleryId=".$companyId."&titleId=".$artwork->MPTitleID."&whichimage=".$i;
		
		$filename = $artwork->URLSegment.'-'.$i.'.jpg';
		
		MasterpieceUtils::grab_image($url,$filename);
		
		if(file_exists($filename)) {
			echo " image written<br>";
			if (copy($filename, "$base/assets/$saveto/$filename")) {
				echo " image copied<br>";
				unlink($filename);
				echo " image deleted<br>";
				
				if(!Image::get()->filter(array("Filename" => "assets/$saveto/$filename"))->count()){
	                $image = new Image();
	                $image->Filename = "assets/$saveto/$filename";
	                if($i > 1) {
		                $image->Title = $artwork->Title.' Detail '.$i;
	                } else {
		                $image->Title = $artwork->Title;
	                }
	                
	                
	                $image->ParentID = $folder->ID;
	                echo "Create Image";
	                $image->write();
	                
					return $image;
                }
				
			}
		}
		
		return false;
		
		
	}
	
}