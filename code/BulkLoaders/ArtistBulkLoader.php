<?php
class ArtistBulkLoader extends CsvBulkLoader {
   public $columnMap = array(
      'MPArtistID' => 'MPArtistID',
      "ShortBio" => "ShortBio"
   );
   public $duplicateChecks = array(
      'MPArtistID' => 'MPArtistID'
   );
}