<?php
	
class ContactAdmin extends ModelAdmin{
	
	private static $managed_models = array(
		'Contact',
		'ContactGroup',
	);

	static $url_segment = 'contacts';
	static $menu_title = 'Contacts';
	static $menu_icon = 'melete/images/contacts.png';
	
}