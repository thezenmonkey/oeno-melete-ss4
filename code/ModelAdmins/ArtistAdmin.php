<?php
	
class ArtistAdmin extends CatalogPageAdmin {
	private static $managed_models = array(
		'Artist'
	);

	static $url_segment = 'artists';
	static $menu_title = 'Artists';
	static $menu_icon = 'melete/images/artist.png';
	
	private static $model_importers = array(
      'Artist' => 'ArtistBulkLoader'
   );
   
   public function getList() {
		$list = parent::getList();
		
		$list = $list->sort(array("IsRepresented" => "DESC",'Contact.LastName' => 'ASC'));
		
		return $list;
	}
   
}