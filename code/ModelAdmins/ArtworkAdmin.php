<?php
	
class ArtworkAdmin extends ModelAdmin {
	private static $managed_models = array(
		'Artwork'
	);

	static $url_segment = 'artwork';
	static $menu_title = 'Artwork';
	static $menu_icon = 'melete/images/artwork.png';

    public function getExportFields() {
        return array(
            'Artist.Title' => 'Artist name',
            'Title' => 'Title',
            'Year' => 'Year',
            'Medium' => 'Medium',
            'MPTitleID' => 'Inventory ID',
            'Images.First.Filename' => 'Filename',
            'Height' => 'Height',
            'Width' => 'Width',
            'Depth' => 'Depth',
            //'' => 'Diameter',
            'Price' => 'Price',
            //'' => 'Image rights',
            //'' => 'Additional information',
            //'' => 'Signature',
            //'' => 'Provenance',
            //'' => 'Exhibition history',
            //'' => 'Literature',
            //'' => 'Series',
            //'' => 'Confidential notes'
        );
    }
}

