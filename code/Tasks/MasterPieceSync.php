<?php

/*
class MasterPieceSync extends BuildTask {
	protected $title = 'MasterpieceSync';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
 
    function run($request) {
		
		$companyId = "1A95-CCGH-6E59";
			    
	    $service = new MasterpieceService("http://".$_SERVER['HTTP_HOST'].Director::BaseURL().MELETE_DIR."/code/ThirdParty/masterpiece.wsdl");
		
        echo "I'm trying to say hi...";
    }
}



class MasterPieceArtistSync extends BuildTask {
	protected $title = 'MasterpieceArtistSync';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
 
    function run($request) {
		
		$companyId = "1A95-CCGH-6E59";
			    
	    $service = new MasterpieceService("http://".$_SERVER['HTTP_HOST'].Director::BaseURL().MELETE_DIR."/code/ThirdParty/masterpiece.wsdl");
		
		$artists = new ArtistRecordArray();
		$artists = $service->GetArtistRecords($companyId,"");
		
		
		$artistHolder = SiteTree::get_by_link("artists");
		
		if($artistHolder) {
			echo "Artist Page is ".$artistHolder->ID."<br>\n ";
		}
		
				
		foreach ($artists as $a) {
			if ( !empty($a->Error) )
				throw new Exception($a->Error);
			
			if( (!$a->FirstName && !$a->LastName) || $a->LastName == "--" ) {
				continue;
			}
			
			//FIND OR CREATE CONTACT
			if($a->LastName == '') {
				echo "No LastName<br>\n ";
				$contact = Contact::get()->where("`FirstName` = '".$a->FirstName."' and `LastName` IS NULL")->first();
			} elseif ($a->FirstName == '') {
				echo "No FirstName<br>\n ";
				$contact = Contact::get()->where("`FirstName` IS NULL AND `LastName` = '".$a->LastName."'")->first();
			} else {
				$contact = Contact::get()->filter(array("FirstName" => $a->FirstName, "LastName" => $a->LastName))->first();
			}
			
			
			if(!$contact) {
				echo "Create New Contact<br>\n ";
				$contact = new Contact();
				
				$contact->FirstName = $a->FirstName;
				$contact->LastName = $a->LastName;
				
				$contact->write();
			} else {
				echo "Contact ".$contact->ID." Found<br>\n ";
			}
			
			//FIND OR CREATE ARTIST
			
			$artistPage = Artist::get()->filter(array("ContactID" => $contact->ID))->first();
			
			if(!$artistPage) {
				echo "Create New Artist<br>\n ";
				
				$artistPage = new Artist();
				
				$artistPage->MPArtistID = $a->ArtistId;
				$artistPage->MPDateAdded = $a->DateAdded;
				$artistPage->MPDateUpdated = $a->DateUpdated;
				$artistPage->MPHasImage = $a->HasImage;
				$artistPage->MPHasBio = $a->HasBio;
				$artistPage->MPDescription = $a->Description;
				$artistPage->MPFirstTitle = $a->FirstTitleId;
				
				$artistPage->ContactID = $contact->ID;
				$artistPage->ParentID = $artistHolder->ID;
				
				$a->Born ? $artistPage->BirthDate = $a->Born : false;
				$a->Died ? $artistPage->DeathDate = $a->Died : false;
				
				$artistPage->write();
				echo "Written Artist<br>\n ";
				$artistPage->publish('Stage', 'Live');
				$artistPage->flushCache();
				//Debug::show($artistPage);
				
			}
			
		}
		
		
		
        echo "I'm trying to say hi...";
    }
    
    
    
	
	 function grab_html($url){
		$ch = curl_init ($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		$raw=curl_exec($ch);
		
		curl_close ($ch);
		
		return $raw;
		
	}
}


class MasterPieceArtistPhotos extends BuildTask {
	protected $title = 'MasterPieceArtistPhotos';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    
 
    function run($request) {
		$artistsWithPhoto = Artist::get()->filter(array("MPHasImage" => 1));
		
		$companyId = "1A95-CCGH-6E59";
		
		$base = Director::baseFolder();
			
		if($artistsWithPhoto->count()) {
			
			foreach($artistsWithPhoto as $artist) {
				set_time_limit ( 60 );
				$url = "http://masterpiecesolutions.org/common/imgbio.php?galleryId=".$companyId."&artistId=".$artist->MPArtistID."&new_width=1200&new_height=1200";
				
				$saveto = "Artists/".$artist->URLSegment;
				 
				$folder = Folder::find_or_make($saveto);
				$filename = $artist->URLSegment.'-picture.jpg';
				echo $filename;
				
				MasterpieceUtils::grab_image($url,$filename);
				
				if(file_exists($filename)) {
					echo " image written<br>\n ";
					if (copy($filename, "$base/assets/$saveto/$filename")) {
						echo " image copied<br>\n ";
						unlink($filename);
						echo " image deleted<br>\n ";
						
						if(!Image::get()->filter(array("Filename" => "assets/$saveto/$filename"))->count()){
			                $image = new Image();
			                $image->Filename = "assets/$saveto/$filename";
			                $image->Title = $artist->Contact()->FirstName.' '.$artist->Contact()->LastName.' Photo';
			                
			                $image->ParentID = $folder->ID;
			                echo "Create Image";
			                $image->write();
			                
			                $artist->PictureID = $image->ID;
			                
			                $artist->write();
			                echo "Written Artist<br>\n ";
							$artist->publish('Stage', 'Live');
							$artist->flushCache();
			                
			                $image->destroy();
		                }
						
					}
				}
				
				
			}
			
			
			
		}
	}
	
}


class MasterPieceArtworks extends BuildTask {
	protected $title = 'MasterPieceArtworks';
	
	protected $description = 'Sync with Masterpiece v9 API';
	
	protected $enabled = true;
	
	
	
	function run($request) {
		
		$companyId = "1A95-CCGH-6E59";
		
		$base = Director::baseFolder();
			    
	    $service = new MasterpieceService("http://".$_SERVER['HTTP_HOST'].Director::BaseURL().MELETE_DIR."/code/ThirdParty/masterpiece.wsdl");
		
		$titles = new TitleRecordArray();
		$titles = $service->GetTitleRecords($companyId,"");
		
		foreach ($titles as $t) {
			if ( !empty($t->Error) )
				throw new Exception($t->Error);
			
			//FIND ARTIST 
			
			$artist = Artist::get()->filter(array("MPArtistID" => $t->ArtistId))->first();
			
			if($artist) {
				echo "artist found<br>\n ";
				$artworkPage = Artwork::get()->filter(array("MPTitleID" => $t->TitleId))->first();
				
				if(!$artworkPage) {
					echo "Create New Artwork<br>\n ";
				
					$artworkPage = new Artwork();
					
					$artworkPage->MPTitleID	  	= $t->TitleId;
					$artworkPage->MPArtistID  	= $t->ArtistId;
					$artworkPage->MPScanCode  	= $t->ScanCode;
					$artworkPage->MPArtCode	  	= $t->ArtCode;
					$artworkPage->MPTitleText1	= $t->TitleText1;
					$artworkPage->MPTitleText2	= $t->TitleText2;
					$artworkPage->MPTitleText3	= $t->TitleText3;
					$artworkPage->MPCustom1	  	= $t->Custom1;
					$artworkPage->MPCustom1	  	= $t->Custom1;
					$artworkPage->MPCustom3	  	= $t->Custom3;
					$artworkPage->MPDateAdded 	= $t->DateAdded;
					$artworkPage->MPDateUpdate	= $t->DateUpdated;
					$artworkPage->MPImages		= $t->Images;
					
					($t->DimUnits == "E") ? $artworkPage->Units = "Imperial" : $artworkPage->Dimensions = "Metric";
					($t->DimType == 3) ? $artworkPage->Dimensions = "3D" : $artworkPage->Dimensions = "2D";
					
					$artworkPage->Title 		= $t->Title;
					$artworkPage->Price		 	= $t->Price;
					$artworkPage->Year		 	= $t->Circa;
					$artworkPage->Medium	 	= $t->Medium;
					$artworkPage->Subject	 	= $t->Subject;
					$artworkPage->OnDisplay	 	= 1;
					$artworkPage->Width		 	= $t->Width;
					$artworkPage->Height	 	= $t->Height;
					$artworkPage->Depth		 	= $t->Depth;
					$artworkPage->EditionType	= $t->EditionType;
					$artworkPage->EditionSize	= $t->EditionSize;
					$artworkPage->Quantity	 	= $t->Quantity;
					$artworkPage->FramePrice 	= $t->FramePrice;
					$artworkPage->FrameWidth 	= $t->FrameHeight;
					$artworkPage->FrameHeight	= $t->FrameWidth;
					
					(strlen($t->Description) > 0) ? $artworkPage->Content = "<p>".$t->Description."</p>" : false;
					
					$artworkPage->ParentID = $artist->ID;
					
					set_time_limit ( 60 );
					$artworkPage->write();
					echo "Written Artwork<br>\n ";
					$artworkPage->publish('Stage', 'Live');
					$artworkPage->flushCache();
					
					//Debug::show($artworkPage);
					
					$imageCount = $t->Images;
					
					if($imageCount > 0) {
						echo "Artwork has Image<br>\n ";
						Debug::show($t);
						$saveto = "Artists/".$artist->URLSegment;
						
						$folder = Folder::find_or_make($saveto);
						
						$i = 1;
						
						while ($i <= $imageCount) {
							set_time_limit ( 60 );
							$url = "http://masterpiecesolutions.org/common/imgpiece.php?galleryId=".$companyId."&titleId=".$t->TitleId."&whichimage=".$i;
							$filename = $artworkPage->URLSegment.'-'.$i.'.jpg';
							
							echo $filename;	
				
							$this->grab_image($url,$filename);
							
							if(file_exists($filename)) {
								echo " image written<br>\n ";
								if (copy($filename, "$base/assets/$saveto/$filename")) {
									echo " image copied<br>\n ";
									unlink($filename);
									echo " image deleted<br>\n ";
									
									if(!Image::get()->filter(array("Filename" => "assets/$saveto/$filename"))->count()){
						                $image = new Image();
						                $image->Filename = "assets/$saveto/$filename";
						                
						                if ($i == 1) {
							                $image->Title = $artistPage->Title;
						                } else {
							                $image->Title = $artistPage->Title.' Detail';
						                }
						                
						                
						                $image->ParentID = $folder->ID;
						                echo "Create Image";
						                $image->write();
						                
						                $artworkPage->Images()->add($image);
						                
						                $image->destroy();
					                }
									
								}
							}
							
							$artworkPage->write();
			                echo "Written Artwork<br>\n ";
							$artworkPage->publish('Stage', 'Live');
							$artworkPage->flushCache();
							
							
							$i++;
						}
				
						
						
						
					} else {
						echo "Artwork has No Image<br>\n ";
					}
					
				}
				
				
				
				
			} else {
				echo "No Artist found<br>\n ";
				continue;
			}
			
			
			//FIND OR CREATE ARTWORK
			//$artistPage = Artist::get()->filter(array("ContactID" => $contact->ID))->first();
			
		}
	    
	}
	function grab_image($url,$saveto){
		$ch = curl_init ($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		$raw=curl_exec($ch);
		curl_close ($ch);
		if(file_exists($saveto)){
		    unlink($saveto);
		}
		$fp = fopen($saveto,'x');
		fwrite($fp, $raw);
		fclose($fp);
	}
	
}


class MasterPieceArtworkImageCheck extends BuildTask {
	protected $title = 'MP Artwork Image Check';
	
	protected $description = 'Sync with Masterpiece v9 API';
	
	protected $enabled = true;
	
	
	
	function run($request) {
		$artworks = Artwork::get();
		
		$base = Director::baseFolder();
		
		if($artworks->count()) {
			
			foreach ($artworks as $art) {
				
				if($art->Images()->count() < $art->MPImages) {
					echo "<b>Missing Images</b><br>\n ";
					
					$saveto = "Artists/".$art->Parent()->URLSegment;
					
					echo $saveto."<br>\n ";
					
					$folder = Folder::find_or_make($saveto);
					
					$i =$art->Images()->count();
						
					while ($i <= $art->MPImages) {
						$url = "http://masterpiecesolutions.org/common/imgpiece.php?galleryId=".$companyId."&titleId=".$t->TitleId."&whichimage=".$i;
						$filename = $art->URLSegment.'-'.$i.'.jpg';
						
						echo $filename;	
			
						$this->grab_image($url,$filename);
						
						if(file_exists($filename)) {
							echo " image written<br>\n ";
							if (copy($filename, "$base/assets/$saveto/$filename")) {
								echo " image copied<br>\n ";
								unlink($filename);
								echo " image deleted<br>\n ";
								
								if(!Image::get()->filter(array("Filename" => "assets/$saveto/$filename"))->count()){
					                $image = new Image();
					                $image->Filename = "assets/$saveto/$filename";
					                
					                if ($i == 1) {
						                $image->Title = $art->Title;
					                } else {
						                $image->Title = $art->Title.' Detail';
					                }
					                
					                
					                $image->ParentID = $folder->ID;
					                echo "Create Image";
					                $image->write();
					                
					                $art->Images()->add($image);
					                
					                $image->destroy();
				                }
								
							}
						}
						
						$art->write();
		                echo "Written Artwork<br>\n ";
						$art->publish('Stage', 'Live');
						$art->flushCache();
						
						$i++;
					}
					
				} else {
					echo "Images Complete<br>\n ";
				}
				
				
			}
			
		}
	}
	
	function grab_image($url,$saveto){
		$ch = curl_init ($url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		$raw=curl_exec($ch);
		curl_close ($ch);
		if(file_exists($saveto)){
		    unlink($saveto);
		}
		$fp = fopen($saveto,'x');
		fwrite($fp, $raw);
		fclose($fp);
	}
}


class MasterPieceEventSync extends BuildTask {
	protected $title = 'Masterpiece Event Sync';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
 
    function run($request) {

		$companyId = "1A95-CCGH-6E59";
		
		$service = new MasterpieceService("http://".$_SERVER['HTTP_HOST'].Director::BaseURL().MELETE_DIR."/code/ThirdParty/masterpiece.wsdl");
		
		$exhibitHolder = SiteTree::get_by_link("exhibitions");
		
		if($exhibitHolder) {
			echo "Ehxibitions Page is ".$exhibitHolder->ID."<br>\n ";
		} else {
			echo "No Holder<br>\n ";
			break;
		}
		
		
		$events = new EventRecordArray();
		$events = $service->GetEventRecords($companyId,'');
		//var_dump( $events );
		
		foreach ($events as $evt) {
			if ( !empty($evt->Error) )
				throw new Exception($evt->Error);
				
			$events = new EventRecordArray();
			$events = $service->GetEventRecords($companyId,'');
			//var_dump( $events );
			
			foreach ($events as $evt) {
				set_time_limit ( 60 );
				if ( !empty($evt->Error) )
					throw new Exception($evt->Error);
				
				$eventPage = Exhibit::get()->filter(array("MPEventID" => $evt->EventId))->first();
			
				if(!$eventPage) {
					echo "Create New Exhibit<br>\n ";
					
					$eventPage = new Exhibit();
					
					$eventPage->Title = $evt->EventTitle;
					$eventPage->StartDate = $evt->StartDate;
					$eventPage->EndDate = $evt->EndDate;
					$eventPage->Content = "<p>".$evt->Description."</p>";
					$eventPage->ParentID = $exhibitHolder->ID;
					
					$eventPage->MPDateAdded 	= $evt->DateAdded;
					$eventPage->MPDateUpdated	= $evt->DateUpdated;
					$eventPage->MPEventID		= $evt->EventId;
					$eventPage->MPDescription	= $evt->Description;
					$eventPage->MPLocation 		= $evt->Location;
					$eventPage->MPFeatured 		= $evt->Featured;
					$eventPage->MPEventTite 	= $evt->EventTitle;
					
					$eventPage->write();
					echo "Written Exhibittion<br>\n ";
					$eventPage->publish('Stage', 'Live');
					
					//Find Art
					$artworks = Artwork::get()->filter(array("MPTitleText3:PartialMatch" => $evt->Featured));
					
					if($artworks->count()) {
						foreach ($artworks as $art) {
							$eventPage->Artworks()->add($art);
							echo  $art->Title." Added<br>\n ";
						}
					}
					$eventPage->write();
					echo "Written Exhibittion<br>\n ";
					$eventPage->publish('Stage', 'Live');
					$eventPage->flushCache();
					
				
				}	
				echo "<br>\n event: " . $evt->EventTitle;
			}
		}
	
	}
}
*/


/*
class FixArtistTags extends BuildTask {
	protected $title = 'Fix Artist Tags';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    
    function run($request) {
	    $artists = Artist::get()->exclude("MPDescription", null);
	    
	    if($artists->count()) {
		    
		    foreach($artists as $artist) {
			    
			    $tags = $artist->MPDescription;
			    
			    $this->extend('ArtistTags', $artist);
			    
			    $artist->write();
			    
		    }
		    
		    
	    }
	}
    
}
*/


class FixArtworkTags extends BuildTask {
	protected $title = 'Fix Artwork Tags';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    
    function run($request) {
	    $artworks = Artwork::get()->exclude("MPDescription", null);
	    
	    if($artworks->count()) {
		    
		    foreach($artworks as $artwork) {
			    
			    $tags = $artwork->MPDescription;
			    
			    $this->extend('ArtworkTags', $artwork);
			    
			    Debug::show($artist->NoPrice);
			    
			    $artist = MasterpieceUtils::TagProcessor($artist);
			    
		    }
		    
		    
	    }
	}
    
}

class MasterPieceUpdateArtists extends BuildTask {
	protected $title = 'Update Artists';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    function run($request) {
	    
		$emailreport = EmailReport::FindOrCreateReport(false, true);
		
		$starttime =  microtime(true);
		
		$yesterday = date('Y-m-d h:i:s',strtotime("-16 hours"));
		
		if(!$emailreport->Title) {
			$emailreport->Title = "Masterpiece Sync for ".$yesterday;
		}
		
		if(!$emailreport->Content) {
			$emailreport->Content = "Getting all changes since ".$yesterday;
		}
		
		$emailreport->write();
				
		
		
		echo "Getting Changes Since ".$yesterday."<br>\n ";
		
		$changes = MasterpieceUtils::getMasterpeiceItem("Artist", " AND dDateUpdated >= '".$yesterday."'");
		$news = MasterpieceUtils::getMasterpeiceItem("Artist", " AND dDateAdded >= '".$yesterday."'");
		
		if($changes) {
			$entry = new ReportEntry();
			$entry->Title = "Artist Updates";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			foreach($changes as $change) {
				if ( !empty($change->Error) ) {
					echo "error";
					
					$entry .= "<br>\n <strong>Error</strong><br>\n ".$change->Error;
					$entry->write();
					
					throw new Exception($change->Error);
				}
				
				if( (!$change->FirstName && !$change->LastName) || $change->LastName == "--" ) {
					echo "No Changes";
					
					$entry .= "<br>\n <strong>No Changes</strong><br>\n ";
					$entry->write();
					continue;
				}
				
				$artist = Artist::get()->filter(array("MPArtistID" => $change->ArtistId))->first();
				
				echo "Artist ID=".$change->ArtistId."<br>";
				
				if(!$artist){
					echo "Artist Not Found<br>\n";
					$artist = MasterpieceUtils::createArtist($change);
					$name = $artist->Title;
					$link = $artist->AbsoluteLink();
					$entry->Content .= "<br>\n Added <a href='$link'>$name</a>";
					$entry->write();
					
				} else {
					
					$currentPicture = $artist->MPHasImage;
					$currentFirstWork = $artist->MPFirstTitle;
					
					MasterpieceUtils::ArtistBridge($artist, $change);
					
					//GET NEW PICTURE
					if($currentPicture && $artist->PictureID != 0) {
						$picture = Image::get()->byID($artist->PictureID);
						if($picture) {
							$picture->delete();
						}
						
					}
					if($currentPicture) {
						$image = MasterpieceUtils::GetArtistPhoto($artist);
						
						if($image) {
							$artist->PictureID = $image->ID;
						} else {
							echo "Image Failed to Download<br>\n ";
						}
					}
					
					//Get new FIrst ITitle
					if($currentFirstWork != $artist->MPFirstTitle) {
						$artwork = Artwork::get()->filter(array("MPTitleID" => $artist->MPFirstTitle))->first();
			
						if(!$artwork) {
							
							$artwork = MasterpieceUtils::CreateArtworkFromID($artist->MPFirstTitle, $artist);
							
						}
					}
					
					//TODO REFRACTOR THIS SECTION
		
					if( stripos($artist->MPDescription,"@no-price@") !== false ) {
						$artist->NoPrice = 1;
					}
					
					
					//find video tag
					if( stripos($artist->MPDescription,"@video-start@") !== false ) {
						echo $artist->Title." New Video<br>\n ";
						$pattern = "/@video-start@(.*?)@video-end@/";
						preg_match_all($pattern, $artist->MPDescription, $matches);
						$i = 1;
						foreach ($matches[1] as $videoURL) {
							echo $videoURL."<br>\n ";
							$exists = Video::get()->filter(array("URL" => $videoURL, "ArtistID" => $artist->ID))->first();
							if(!$exists) {
								echo $videoURL."<br>\n ";
								$video = new Video();
								$video->Title = $artist->Title." ".$i;
								$video->URL = $videoURL;
								$video->ArtistID = $artist->ID;
								echo "Creating ".$videoURL." New Video<br>\n ";
								$video->write();
							} else {
								Debug::show($exists);
							}
							
							$i++;
						}
						
					}
					
					//find secondary tag
					if( stripos($artist->MPDescription,"@secondary@") !== false ) {
						$artist->Secondary = 1;
					}
					
					//find secondary tag
					if( stripos($artist->MPDescription,"@outdoor@") !== false ) {
						$artist->Outdoor = 1;
					}
					
					$artist->write();
					echo "Updated Artist<br>\n ";
					
					//$artist = MasterpieceUtils::createArtist($change);
					$name = $artist->Title;
					$link = $artist->AbsoluteLink();
					$entry->Content .= "<br>\n Updated <a href='$link'>$name</a>";
					$entry->write();
					
					$artist->publish('Stage', 'Live');
					$artist->flushCache();
				}
					
			}
		}
		
		if($news) {
			
			$entry = new ReportEntry();
			$entry->Title = "Artists Added";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			foreach($news as $new) {
				if ( !empty($new->Error) ) {
					echo "error";
					
					$entry .= "<br>\n <strong>Error</strong><br>\n ".$new->Error;
					$entry->write();
					throw new Exception($new->Error);
				}
				
				if( (!$new->FirstName && !$new->LastName) || $new->LastName == "--" ) {
					echo "No New";
					$entry .= "<br>\n <strong>No Chages</strong><br>\n ";
					$entry->write();
					continue;
				}
				
				$artist = Artist::get()->filter(array('MPArtistID' => $new->ArtistId))->first();
				// CREATE ARTIST
				if(!$artist) {
					
					$artist = MasterpieceUtils::createArtist($new);
					
					$name = $artist->Title;
					$link = $artist->AbsoluteLink();
					$entry->Content .= "<br>\n Added <a href='$link'>$name</a>";
					$entry->write();
					
				}
				
					
			}
		}
		
		$fullList =  MasterpieceUtils::getMasterpeiceItem("Artist", "");
		
		if($fullList) {
			
			echo "Processing Existing Artists<br>\n ";
			
			$entry = new ReportEntry();
			$entry->Title = "Artists Removed";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			//print_r($fullList);
			
			$exists = array();
			echo "List ".count($fullList);
			echo "<br>\n <br>\n ";
			foreach($fullList as $listItem) {
				
				if ( !empty($listItem->Error) ){
					throw new Exception($listItem->Error);
					print_r($listItem->Error);
				}
				
				if( !$listItem->ArtistId || $listItem->ArtistId == "0" ) {
					echo "No Changes";
					continue;
				}
				
				//Add Title ID to MasterList Array
				$exists[] = $listItem->ArtistId;
					
			}
			
			
			//Get an Query of just ArtworkIDs and TitleIDa
			$sqlQuery = new SQLQuery();
			$sqlQuery->setFrom('Artist');
			$sqlQuery->setSelect('ID');
			$sqlQuery->addSelect('MPArtistID');
			
			// Get the raw SQL (optional)
			$rawSQL = $sqlQuery->sql();
			
			// Execute and return a Query object
			$results = $sqlQuery->execute();
			
			
			//TODO: Check removal logic
			foreach ($results as $result) {
				
				if(!in_array($result['MPArtistID'], $exists)) {
					echo $result['MPArtistID']. "not in array<br>\n ";
					
					$artist = Artist::get()->byID($result['ID']);
					
					
					
					if($artist && !$artist->ViewerGroups()->count()) {
						$groups = Group::get()->filterAny(array("Code" => "administrators", "Code" => "content-authors"));
						echo "everyone can see<br>\n ";
						foreach($groups as $group) {
							$artist->ViewerGroups()->add($group);
						}
						
//						$artist->write();
//						$artist->doPublish();
//						$artist->flushCache();
						echo "Permission Change<br>\n ";
						
						$entry->Content .= "<br>\n ".$artist->Title." and Art Removed From Site (Data Retained)";
						
						$hasArts = $artist->Artworks()->filter(array("Quantity:GreaterThan" => 0));
						if ($hasArts->count()) {
							echo "Has Art<br>\n ";
							foreach ($hasArts as $hasArt) {
								echo "Count to 0<br>\n ";
								$hasArt->Quantity = 0;
								$hasArt->write();
							}
							
						}
						
					}
					
				}
				
			}
			
			$entry->write();
			
		}
		
		$endtime = microtime(true);
		
		$totaltime = $endtime - $starttime;
		
		$entry->Content .= "<br>\n <strong>Artist Sync Completed at ".date('G:i').'</strong>';
		$entry->Content .= "<br>\n <strong>Total Time = $totaltime</strong>";
		$entry->write();
		
	}
	
	
    
}


//TODO ADD TAG PROCESSOR
class MasterPieceUpdateArtwork extends BuildTask {
	protected $title = 'Update Artwork';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    function run($request) {
		$starttime =  microtime(true);
		
		$emailreport = EmailReport::FindOrCreateReport(false, true);

		$yesterday = date('Y-m-d h:i:s',strtotime("-16 hours"));

		if(!$emailreport->Title) {
			$emailreport->Title = "Masterpiece Sync for ".$yesterday;
		}

		if(!$emailreport->Content) {
			$emailreport->Content = "Getting all changes since ".$yesterday;
		}

		$emailreport->write();
		
		echo "Getting Changes Since ".$yesterday."<br>\n ";
		
		$changes = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND (title.dDateUpdated >= '".$yesterday."' OR item.dDateUpdated >= '".$yesterday."')");
		$news = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND title.dDateAdded >= '".$yesterday."'");
		
		
		if($news) {
			//Debug::show($news);
			
			$entry = new ReportEntry();
			$entry->Title = "New Artwork";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			foreach($news as $new) {
				//Debug::show($new);
				if ( !empty($new->Error) ) {
					
					$entry .= "<br>\n <strong>Error</strong><br>\n ".$new->Error;
					$entry->write();
					
					throw new Exception($new->Error);
				}
				
				if( !$new->TitleId || $new->TitleId == "0" ) {
					
					$entry .= "<br>\n <strong>No New Art</strong><br>\n ";
					$entry->write();
					
					echo "No New";
					continue;
				}
				
				$artworkPage = Artwork::get()->filter(array("MPTitleID" => $new->TitleId))->first();
				$artist = MasterpieceUtils::findOrCreateArtist($new->ArtistId);
				
								
				if(!$artworkPage && $artist) {
					echo "No Artwork<br>\n ";
					echo "Creating ".$new->Title."<br>\n ";
					$artworkPage = MasterpieceUtils::createArtwork($new, $artist);
					
					$artworkPage->write();
					echo "Artwork Publish<br>\n ";
					
					$name = $artworkPage->Title;
					$link = $artworkPage->AbsoluteLink();
					$artistName = $artist->Title;
					$entry->Content .= "<br>\n Added <a href='$link'>$name</a> by $artistName";
					$entry->write();
					
					
					
						
				} else {
					echo $artworkPage->Title." Already Exists<br>\n ";
				}
			}
		}
		
		
		if($changes) {
			echo "Processing Changes<br>\n ";
			$entry = new ReportEntry();
			$entry->Title = "Changed Artwork";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			
			foreach($changes as $change) {
				//Debug::show($change);
				if ( !empty($change->Error) ) {
					$entry .= "<br>\n <strong>Error</strong><br>\n ".$new->Error;
					$entry->write();
					
					throw new Exception($change->Error);
				}

				
				if( !$change->TitleId || $change->TitleId == "0" ) {
					echo "No Changes";
					$entry .= "<br>\n <strong>No Changes</strong><br>\n ";
					$entry->write();
					continue;
				}
				
				echo $change->TitleId.'<br>\n ';
				$artworkPage = Artwork::get()->filter(array("MPTitleID" => $change->TitleId))->first();
				
				if(!$artworkPage) {
					echo "No Artwork<br>\n ";
					$artist = Artist::get()->filter(array("MPArtistID" => $change->ArtistId))->first();
					$artworkPage = MasterpieceUtils::createArtwork($change, $artist);
					echo "Created ".$artworkPage->Title;
					
					$name = $artworkPage->Title;
					$link = $artworkPage->AbsoluteLink();
					$artistName = $artist->Title;
					$entry->Content .= "<br>\n Added <a href='$link'>$name</a> by $artistName";
					$entry->write();
					
					
				} else {
					echo "Changing ".$artworkPage->Title.'<br>\n ';
					MasterpieceUtils::ArtworkBridge($artworkPage, $change);
					
					
					//DELETE EXISTING IMAGES JUST IN CASE ITS THE CHANGE
					if($artworkPage->Images()->count() && $artworkPage->Quantity != 0) {
						echo "Deleting Existing Images";
						foreach($artworkPage->Images() as $image) {
							$image->delete();
						}
						
						
						if($artworkPage->MPImages > 0) {
							echo "Artwork has Image<br>\n ";
							
							$imageCount = $artworkPage->MPImages;
							
							$i = 1;
							
							while ($i <= $imageCount) {
								
								$image = MasterpieceUtils::GetArtworkImage($artworkPage, $i);
								
								if($image) {
									$artworkPage->Images()->add($image);
									$image->destroy();
								}
								
								$i++;
								
							}
						}
						
					}
					
					
					$artworkPage->write();
					
					$name = $artworkPage->Title;
					$link = $artworkPage->AbsoluteLink();
					$artistName = $artworkPage->Artist()->Title;
					$entry->Content .= "<br>\n Updated <a href='$link'>$name</a> by $artistName";
					$entry->write();
					
					echo "Artwork Publish<br>\n ";
					//$artworkPage->publish('Stage', 'Live');
					//$artworkPage->flushCache();
					
					
				}
				
				
			}
		}
		
		set_time_limit(120);
		//get Full List
		
		$fullList =  MasterpieceUtils::getMasterpeiceItem("Artwork", "");
		
		if($fullList) {
			echo "Processing Existing Artwork<br>\n ";
			
			$entry = new ReportEntry();
			$entry->Title = "Removed Artwork";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			//print_r($fullList);
			
			$exists = array();
			echo "List ".count($fullList);
			echo "<br>\n <br>\n ";
			foreach($fullList as $listItem) {
				if ( !empty($listItem->Error) ){
					throw new Exception($listItem->Error);
					print_r($listItem->Error);
				}
				
				if( !$listItem->TitleId || $listItem->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
				
				//Add Title ID to MasterList Array
				if($listItem->Quantity != 0) {
					$exists[] = $listItem->TitleId;
				} /*else {
					echo ' <br>\n '.$listItem->Title.' '.$listItem->TitleId.' is 0';
				}*/
				
					
			}
			
			
			//Get an Query of just ArtworkIDs and TitleIDa
			$sqlQuery = new SQLQuery();
			$sqlQuery->setFrom('Artwork');
			$sqlQuery->setSelect('ID');
			$sqlQuery->addSelect('MPTitleID');
			$sqlQuery->addWhere("Quantity > 0");
			
			// Get the raw SQL (optional)
			$rawSQL = $sqlQuery->sql();
			
			// Execute and return a Query object
			$results = $sqlQuery->execute();
			
			echo count($exists);
			if(count($exists) != 0) {
				echo '<br>\n  Do Clean Up';
				foreach ($results as $result) {
					
					if(!in_array($result['MPTitleID'], $exists)) {
						echo $result['MPTitleID']. ' not in array<br>\n ';
						$artwork = Artwork::get()->byID($result['ID']);
						
						if($artwork) {
							$artwork->Quantity = 0;
							$artwork->write();
							
							$entry->Content .= "<br>\n Setting ".$artwork->Title.' by '.$artwork->Artist()->Title.' to 0';
							
						}
						
					}
					
				}
			
			}
			
			$entry->write();
			
		}
		
		$endtime = microtime(true);
		
		$totaltime = $endtime - $starttime;
		
		$entry->Content .= "<br>\n <strong>Artwork Sync Completed at ".date('G:i').'</strong>';
		$entry->Content .= "<br>\n <strong>Total Time = $totaltime</strong>";
		$entry->write();
		
		echo '<br>\n  Done';
		
		
	}
    
}

class MasterPieceUpdateEvents extends BuildTask {
	protected $title = 'Update Events';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
 
    function run($request) {
	    
	    $starttime =  microtime(true);

        $entry = false;
	    
	    $emailreport = EmailReport::FindOrCreateReport(false, true);

		$yesterday = date('Y-m-d h:i:s',strtotime("-16 hours"));

		if(!$emailreport->Title) {
			$emailreport->Title = "Masterpiece Sync for ".$yesterday;
		}

		if(!$emailreport->Content) {
			$emailreport->Content = "Getting all changes since ".$yesterday;
		}
		
		$emailreport->write();
		
		echo "Getting Changes Since ".$yesterday."<br>\n ";
		
		$changes = MasterpieceUtils::getMasterpeiceItem("Event", " AND dUpdated >= '".$yesterday."'");
		$news = MasterpieceUtils::getMasterpeiceItem("Event", " AND dAdded >= '".$yesterday."'");
		
		
		if($news) {
			$entry = new ReportEntry();
			$entry->Title = "New Exhibits";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			Debug::show($news);
			foreach($news as $new) {
				if ( !empty($new->Error) )
					throw new Exception($new->Error);
				Debug::show($new);
				if( !$new->EventId || $new->EventId == "0" ) {
					echo "No New";
					continue;
				}
				
				$eventPage = Exhibit::get()->filter(array("MPEventID" => $new->EventId))->first();
				
				if(!$eventPage) {
					echo "Create New Exhibit<br>\n ";
					
					$exhibit = MasterpieceUtils::createExhibit($new);
					
					
					$artworks = Artwork::get()->filter(array("MPTitleText3:PartialMatch" => $new->Featured));
					
					if($artworks->count()) {
						//ADD ARTWORK ONLY IF ITS NOT ALREADY THERE
						foreach ($artworks as $art) {
							if(!$exhibit->Artworks()->find('ID', $art->ID)) {
								$exhibit->Artworks()->add($art);
								echo  $art->Title." Added<br>\n ";
								
								$name = $exhibit->Title;
								$link = $exhibit->AbsoluteLink();
								$artTitle = $art->Title;
								$artistName = $art->Artist()->Title;
								
								$entry->Content .= "<br>\n $artTitle by $artistName added to <a href='$link'>$name</a>";
								
							}
							
						}
					}
					
					
					$name = $exhibit->Title;
					$link = $exhibit->AbsoluteLink();
					
					$entry->Content .= "<br>\n Added <a href='$link'>$name</a>";
					
					
				}
				
			}
			
			$entry->write();
		}
		
		if($changes) {
			Debug::show($changes);
			
			$entry = new ReportEntry();
			$entry->Title = "Changed Exhibits";
			$entry->Content = '';
			$emailreport->ReportEntries()->add($entry);
			
			foreach($changes as $change) {
				if ( !empty($change->Error) )
					throw new Exception($change->Error);
				
				if( !$change->EventId || $change->EventId == "0" ) {
					echo "No New";
					continue;
				}
				
				$eventPage = Exhibit::get()->filter(array("MPEventID" => $change->EventId))->first();
				
				if(!$eventPage) {
					echo "Create New Exhibit<br>\n ";
					
					$exhibit = MasterpieceUtils::createExhibit($change);
					
					$name = $exhibit->Title;
					$link = $exhibit->AbsoluteLink();
					
					$entry->Content .= "<br>\n Added <a href='$link'>$name</a>";
					
				} else {
					
					MasterpieceUtils::ExhibitBridge($eventPage, $change);
					
					//Find Art
					$artworks = Artwork::get()->filter(array("MPTitleText3:PartialMatch" => $change->Featured));
					
					if($artworks->count()) {
						//ADD ARTWORK ONLY IF ITS NOT ALREADY THERE
						foreach ($artworks as $art) {
							if(!$exhibit->Artworks()->find('ID', $art->ID)) {
								$exhibit->Artworks()->add($art);
								echo  $art->Title." Added<br>\n ";
								
								$name = $exhibit->Title;
								$link = $exhibit->AbsoluteLink();
								$artTitle = $art->Title;
								$artistName = $art->Artist()->Title;
								
								$entry->Content .= "<br>\n $artTitle by $artistName added to <a href='$link'>$name</a>";
								
							}
							
						}
					}
					$eventPage->write();
					echo "Written Exhibittion<br>\n ";
					$eventPage->publish('Stage', 'Live');
					
					$name = $exhibit->Title;
					$link = $exhibit->AbsoluteLink();
					
					$entry->Content .= "<br>\n Updated <a href='$link'>$name</a>";
					
					$entry->write();
					
					$eventPage->flushCache();
					
				}
				
			}
		}
		
		if(!$news && !$changes) {
			$check = MasterpieceUtils::getMasterpeiceItem("Event", "");
			
			if ($check) {
				//Debug::show($check);
			}
		}
		
		$endtime = microtime(true);
		
		$totaltime = $endtime - $starttime;
		
		if(!$entry) {
			$entry = new ReportEntry();
			$emailreport->ReportEntries()->add($entry);
		}
		
		$entry->Content .= "<br>\n <strong>Event Sync Completed at ".date('G:i').'</strong>';
		$entry->Content .= "<br>\n <strong>Total Time = $totaltime</strong>";
		
		$entry->write();
		
		echo "Done";
	    
    }
}


class MasterPieceCategories extends BuildTask {
	protected $title = 'Update Cats';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    function run($request) {
	    $cats = MasterpieceUtils::getMasterpeiceItem("Category", '');
	    
	    $categoryArray = explode(',',$cats[0]->Categories);
		$mediumArray = explode(',',$cats[0]->Mediums);
		

		
		echo "Categories<br>\n ";
		foreach($categoryArray as $cat) {
			$category = MasterpieceUtils::FindOrMakeCategory($cat);
		}
		
		
		
		echo "Mediums<br>\n ";
		//Debug::show($mediumArray);
	}
	
}


class MastepeiceCatSync extends BuildTask {
	protected $title = 'Sync Cats';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    function run($request) {
	    
	    $artworks = Artwork::get()->filter(array('CategoryID' => 0));
	    
	    if ($artworks->count()) {
		    foreach ($artworks as $artwork) {
			    $changes = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND title.iTitleNum = ".$artwork->MPTitleID);
			    
			    if($changes) {
					echo "Processing Changes<br>\n ";
					foreach($changes as $change) {
						//Debug::show($change);
						if ( !empty($change->Error) )
							throw new Exception($change->Error);
						
						if( !$change->TitleId || $change->TitleId == "0" ) {
							echo "No Changes";
							continue;
						}
						
						$cat = MasterpieceUtils::FindOrMakeCategory($change->Category);
						
						if($cat){
							$artwork->CategoryID = $cat->ID;
							
							$artwork->write();
							echo "Artwork Publish<br>\n ";
							$artwork->publish('Stage', 'Live');
							$artwork->flushCache();
							
							
						}
						
					}
				    
			    }
		    }
	    }
	    
	}
}


/*
class CronTest extends BuildTask {
	protected $title = 'CronTest';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
    
    function run($request) {
		
		$email = new Email("server@oenogallery.com", "rick@k2climbing.com", "Cron Test", "Success at ".date('G:i'));
		$email->sendPlain();
	}
}
*/


class ArtistDump extends BuildTask {
	protected $title = 'Artist Dump';
 
    protected $description = 'Pull Entire Artist List From Masterpiece';
 
    protected $enabled = true;
    
   
    
    function run($request) {
	    
	    //$records =  MasterpieceUtils::getMasterpeiceItem("Event", "");
	    
	    $yesterday = date('Y-m-d h:i:s',strtotime("-16 hours"));
	    
	    echo "Getting Changes Since ".$yesterday."<br>\n ";
	    
	    $records = MasterpieceUtils::getMasterpeiceItem("Artist", "");
	    
	    
	    
	    if($records) {
		    echo "records<br>\n ";
		    
		    foreach ($records as $record) {
			    if ( !empty($record->Error) ) {
					throw new Exception($record->Error);
				}
				
				if( !$record->TitleId || $record->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
		    
		    }
		    
		    
	    } else {
		    echo "nothing<br>\n ";
	    }
	    echo "<pre>";
	    print_r($records);
	    echo "</pre>";

	}
}

class ArtworkDump extends BuildTask {
	protected $title = 'Artwork Dump';
 
    protected $description = 'Pull Entire Artwork List From Masterpiece';
 
    protected $enabled = true;
    
   
    
    function run($request) {
	    
	    //$records =  MasterpieceUtils::getMasterpeiceItem("Event", "");
	    
	    $yesterday = date('Y-m-d h:i:s',strtotime("-16 hours"));
	    
	    echo "Getting Changes Since ".$yesterday."<br>\n ";
	    
	    $records = MasterpieceUtils::getMasterpeiceItem("Artwork", "");
	    
	    
	    
	    if($records) {
		    echo "records<br>\n ";
		    
		    foreach ($records as $record) {
			    if ( !empty($record->Error) ) {
					throw new Exception($record->Error);
				}
				
				if( !$record->TitleId || $record->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
		    
		    }
		    
		    
	    } else {
		    echo "nothing<br>\n ";
	    }
	    
	    echo "<pre>";
	    print_r($records);
	    echo "</pre>";

	}
}

class EventDump extends BuildTask {
	protected $title = 'Event Dump';
 
    protected $description = 'Pull Entire Event List From Masterpiece';
 
    protected $enabled = true;
    
   
    
    function run($request) {
	    
	    //$records =  MasterpieceUtils::getMasterpeiceItem("Event", "");
	    
	    $yesterday = date('Y-m-d h:i:s',strtotime("-16 hours"));
	    
	    echo "Getting Changes Since ".$yesterday."<br>\n ";
	    
	    $records = MasterpieceUtils::getMasterpeiceItem("Event", "");
	    
	    
	    
	    if($records) {
		    echo "records<br>\n ";
		    
		    foreach ($records as $record) {
			    if ( !empty($record->Error) ) {
					throw new Exception($record->Error);
				}
				
				if( !$record->TitleId || $record->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
		    
		    }
		    
		    
	    } else {
		    echo "nothing<br>\n ";
	    }
	    
	    echo "<pre>";
	    print_r($records);
	    echo "</pre>";

	}
}


class FixArtworkSync extends BuildTask {
	protected $title = 'Fix Missing Feild In Artwork';
 
    protected $description = 'Sync with Masterpiece v9 API';
 
    protected $enabled = true;
 
    function run($request) {
	    
	    $changes = MasterpieceUtils::getMasterpeiceItem("Artwork", "");
	    
	    if($changes) {
			echo "Processing Changes<br>\n ";
			foreach($changes as $change) {
				//Debug::show($change);
				if ( !empty($change->Error) )
					throw new Exception($change->Error);
				
				if( !$change->TitleId || $change->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
				
				echo $change->TitleId.'<br>\n ';
				$artworkPage = Artwork::get()->filter(array("MPTitleID" => $change->TitleId))->first();
				
				if($artworkPage) {
					echo "Changing ".$artworkPage->Title.'<br>\n ';
					MasterpieceUtils::ArtworkBridge($artworkPage, $change);
					
					
					$artworkPage->write();
					echo "Artwork Publish<br>\n ";
					$artworkPage->publish('Stage', 'Live');
					$artworkPage->flushCache();
					
					
				}
				
				
			}
		}
		
		echo "Done<br>\n ";
	}
}

class TestingAccess extends BuildTask {
	protected $title = 'Testing Section';

    protected $description = 'Holder Class to Test Functions';

    protected $enabled = true;
    
   
    
    function run($request) {
	    
	    //$records =  MasterpieceUtils::getMasterpeiceItem("Event", "");
	    
	    $yesterday = date('Y-m-d h:i:s',strtotime("-16 hours"));
	    
	    echo "Getting Changes Since ".$yesterday."<br>\n ";
	    
	    $records = MasterpieceUtils::getMasterpeiceItem("Artwork", "");
	    
	    
	    
	    if($records) {
		    echo "records<br>\n ";
		    
		    foreach ($records as $record) {
			    if ( !empty($record->Error) ) {
					throw new Exception($record->Error);
				}
				
				if( !$record->TitleId || $record->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
		    
		    }
		    
		    
	    } else {
		    echo "nothing<br>\n ";
	    }
	    
	    print_r($records);
	   	/*
$records = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND title.iTitleNum = 9685");
		
		if($records) {
			
			foreach($records as $record) {
				//Debug::show($change);
				if ( !empty($record->Error) )
					throw new Exception($record->Error);
				
				if( !$record->TitleId || $record->TitleId == "0" ) {
					echo "No Changes";
					continue;
				}
				
				$artwork = Artwork::get()->filter(array("MPTitleID" => 9685))->first();
				
				if(!$artwork) {
					echo "Create New <br>\n ";
					$artwork = new Artwork();
				}
				
				Debug::show($artwork);
				
				print_r($record);
				
				$artwork = MasterpieceUtils::ArtworkBridge($artwork, $record);
				
				
				if($artwork->MPImages > 0) {
					echo "Artwork has Image<br>\n ";
					
					$imageCount = $artwork->MPImages;
					
					$i = 1;
					
					while ($i <= $imageCount) {
						
						$image = MasterpieceUtils::GetArtworkImage($artwork, $i);
						
						if($image) {
							$artwork->Images()->add($image);
							$image->destroy();
						}
						
						$i++;
						
					}
				}
				
				Debug::show($artwork);
			}
			
			set_time_limit ( 30 );
			//$this->write();
			echo "Written Artwork<br>\n ";
			//$this->publish('Stage', 'Live');
			//$this->flushCache();
			
		}
*/
		
		/*
if($fullList) {
			echo "Processing Existing Artwork<br>\n ";
			
			//print_r($fullList);
			
			$exists = array();
			echo "List ".count($fullList);
			echo "<br>\n <br>\n ";
			foreach($fullList as $listItem) {
				
				if ( !empty($listItem->Error) ){
					throw new Exception($listItem->Error);
					print_r($listItem-Error);
				}
				
				if( !$listItem->ArtistId || $listItem->ArtistId == "0" ) {
					echo "No Changes";
					continue;
				}
				
				//Add Title ID to MasterList Array
				$exists[] = $listItem->ArtistId;
					
			}
			
			
			//Get an Query of just ArtworkIDs and TitleIDa
			$sqlQuery = new SQLQuery();
			$sqlQuery->setFrom('Artist');
			$sqlQuery->setSelect('ID');
			$sqlQuery->addSelect('MPArtistID');
			
			// Get the raw SQL (optional)
			$rawSQL = $sqlQuery->sql();
			
			// Execute and return a Query object
			$results = $sqlQuery->execute();
			
			
			
			foreach ($results as $result) {
				
				if(!in_array($result['MPArtistID'], $exists)) {
					echo $result['MPArtistID']. 'not in array<br>\n ';
					
					$artist = Artist::get()->byID($result['ID']);
					
					if($artist && !$artist->ViewerGroups()->count()) {
						$groups = Group::get()->filterAny(array("Code" => "administrators", "Code" => "content-authors"));
						echo "everyone can see<br>\n ";
						foreach($groups as $group) {
							$artist->ViewerGroups()->add($group);
						}
						
						//$artist->write();
						//$artist->doPublish();
						//$artist->flushCache();
						echo "Permission Change<br>\n ";
						$hasArts = $artist->Artworks()->filter(array("Quantity:GreaterThan" => 0));
						if ($hasArts->count()) {
							echo "Has Art<br>\n ";
							foreach ($hasArts as $hasArt) {
								echo "Count to 0<br>\n ";
								$hasArt->Quantity = 0;
								$hasArt->write();
							}
							
						}
						
					}
					
				}
				
			}
			
			
		}
		    
*/
		    
		    
		    /*
$artwork->Title = $artwork->TempTitle;
		    $artwork->Content = $artwork->TempContent;
		    $artwork->URLSegment = $artwork->TempURL;
		    
		    $artwork->Created = $artwork->TempCreated;
		    $artwork->LasteEdited = $artwork->TempLastEdited;
		    
		    $artwork->write();
		    echo "Artwork Saved<br>\n ";
		    
*/
		    
		    
		    /*
$artwork->TempContent = $artwork->Content;
		    $artwork->TempTitle = $artwork->Title;
		    $artwork->ArtistID = $artwork->ParentID;
		    $artwork->TempURL = $artwork->URLSegment;
		    $artwork->TempCreated = $artwork->Created;
		    $artwork->TempLastEdited = $artwork->LasteEdited;
		    
		    
		    $artwork->write();
			echo "Artwork Publish<br>\n ";
			$artwork->publish('Stage', 'Live');
			$artwork->flushCache();
*/
		    		    
	    
	    
	    //$yesterday = date('Y-m-d',strtotime("-4 days"));
		
		//echo "Getting Changes Since ".$yesterday."<br>\n ";
		
		//$changes = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND title.dDateUpdated >= '".$yesterday."'");
		
		//Debug::show($changes);
	    
	}
}

class CheckTitle extends BuildTask {
    protected $title = 'Check Title';

    protected $description = 'Check Data for Specific Title by ID';

    protected $enabled = true;

    function run($request) {

        $params = $request->getVars();

        if(array_key_exists('TitleId', $params)) {
            set_time_limit ( 30 );
            $records = MasterpieceUtils::getMasterpeiceItem("Artwork", "AND title.iTitleNum = ".$params['TitleId']);

            if($records) {

                foreach($records as $record) {
                    //Debug::show($change);
                    if ( !empty($record->Error) )
                        throw new Exception($record->Error);

                    if( !$record->TitleId || $record->TitleId == "0" ) {
                        echo "No Changes";
                        continue;
                    }

                    print_r($record);
                }

            }

            return true;
        }

        return false;

        if(array_key_exists('ArtistId', $params)) {
            set_time_limit ( 30 );
            $records = MasterpieceUtils::getMasterpeiceItem("Artist", "AND title.iTitleNum = ".$params['ArtistId']);

            if($records) {

                foreach($records as $record) {
                    //Debug::show($change);
                    if ( !empty($record->Error) )
                        throw new Exception($record->Error);

                    if( !$record->TitleId || $record->TitleId == "0" ) {
                        echo "No Changes";
                        continue;
                    }

                    print_r($record);
                }

            }

            return true;
        }

        return false;

    }
}

class CheckArtist extends BuildTask {
    protected $title = 'Check Artist';

    protected $description = 'Check Data for Specific Artist by ID';

    protected $enabled = true;

    function run($request) {
        print_r($request);
    }
}