<?php

class GallerySiteConfig extends DataExtension {
	
	private static $db = array(
		"GalleryName" => "Varchar"
	);
	
	private static $has_one = array(
		"Logo" => "Image"
	);
	
	private static $has_many = array(
		
	);
	
	public function updateCMSFields(FieldList $fields) {
		
		$fields->addFieldsToTab("Root.BusinessInformation", 
			array(
				TextField::create("GalleryName"),
				TextField::create("Address"),
				TextField::create("Address2"),
				TextField::create("City"),
				TextField::create("StateOrProvince"),
				TextField::create("Country"),
				TextField::create("PostalCode"),
				TextField::create("Phone"),
				TextField::create("Fax"),
				EmailField::create("MainEmail"),
				UploadField::create("Logo")
			)
		);
		
		
	}
	
	function requireDefaultRecords() {
		$artistFolder = Folder::find_or_make("Artists");
		
	
		parent::requireDefaultRecords();
	}

}