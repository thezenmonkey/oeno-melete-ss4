<?php
	
class Contact extends DataObject {
	
	public function canView($member = null) {
        return Permission::check('CMS_ACCESS_ContactAdmin', 'any', $member);
    }

    public function canEdit($member = null) {
        return Permission::check('CMS_ACCESS_ContactAdmin', 'any', $member);
    }

    public function canDelete($member = null) {
        return Permission::check('CMS_ACCESS_ContactAdmin', 'any', $member);
    }

    public function canCreate($member = null) {
        return Permission::check('CMS_ACCESS_ContactAdmin', 'any', $member);
    }
	
	
	private static $db = array (
		"FirstName" => "Varchar",
		"LastName" => "Varchar",
		"Company" => "Varchar",
		"Email" => "Varchar"
	);
	
	private static $has_one = array (
		"Member" => "Member",
		"ArtistPage" => "Artist"
	);
	
	private static $has_many = array (
		
	);
	
	private static $belongs_many_many = array (
		"ContactGroups" => "ContactGroup"
	);
	
	private static $summary_fields = array (
	   "LastName",
	   "FirstName"
    );
	
	
	
	public function getCMSFields() {
		
		$fields = parent::getCMSFields();
		
		$fields->addFieldToTab('Root.Main', TextField::create('FirstName'), "Address");
		$fields->addFieldToTab('Root.Main', TextField::create('LastName'), "Address");
		$fields->addFieldToTab('Root.Main', TextField::create('Company'), "Address");
		
		if(!$this->MemberID || $this->MemberID == 0) {
			$fields->addFieldToTab('Root.Main', EmailField::create('Email'), "Address");
		} else {
			$fields->addFieldToTab(
				'Root.Main', 
				ReadonlyField::create('Email', 'Email (login)', $this->Member()->Email), 
				"Address"
			);
		}
		
		$groupsMap = array();
		foreach(ContactGroup::get() as $group) {
			// Listboxfield values are escaped, use ASCII char instead of &raquo;
			$groupsMap[$group->ID] = $group->Title;
		}
		asort($groupsMap);
		
		$fields->addFieldToTab('Root.Main', ListboxField::create('ContactGroups', 'Groups')
			->setMultiple(true)
			->setSource($groupsMap)
			->setAttribute(
				'data-placeholder', 
				_t('Member.ADDGROUP', 'Add group', 'Placeholder text for a dropdown')
			));
		
		$fields->removeFieldFromTab("Root.Main", "MemberID");
		
		return $fields;
	}
	
	public function getTitle() {
		return $this->LastName.", ".$this->FirstName;
	}
	
	public function onBeforeWrite() {
		
		if(!$this->MemberID || $this->MemberID == 0) {
			$member = Member::get()->filter(array("Email" => $this->Email))->first();
				
			if($member) {
				$this->MemberID = $member->ID;
			}
			
		} else {
			$member = $this->Member();
		}
		
		parent::onBeforeWrite();
	}
	
}