<?php
	
class Location extends DataObject {
	
	private static $db = array (
		"Title" => "Varchar",
		"Public" => "Boolean",
		"Content" => "Text"
	);
	
	private static $has_one = array (
		"Image" => "Image"
	);
	
	private static $has_many = array (
		"Exhibits" => "Exhibit",
		"Artworks" => "Artwork",
		"Editions" => "Edition"
	);
	
}