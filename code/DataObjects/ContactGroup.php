<?php

class ContactGroup extends DataObject {
	
	private static $db = array (
		"Code" => "Varchar",
		"Title" => "Varchar",
		"Sort" => "Int"
	);
	
	private static $has_one = array (
	);
	
	private static $has_many = array (
		
	);
	
	private static $many_many = array (
		"Contacts" => "Contact"
	);
	
	function requireDefaultRecords() {
		$allGroups = DataObject::get('ContactGroup');
		if(!$allGroups->count()) {
			$artistGroup = new ContactGroup();
			$artistGroup->Code = 'artist';
			$artistGroup->Title = 'Artist';
			$artistGroup->Sort = 0;
			$artistGroup->write();
			
			$clientGroup = new ContactGroup();
			$clientGroup->Code = 'client';
			$clientGroup->Title = 'Client';
			$clientGroup->Sort = 1;
			$clientGroup->write();
			
			$vendorGroup = new ContactGroup();
			$vendorGroup->Code = 'vendor';
			$vendorGroup->Title = 'Vendor';
			$vendorGroup->Sort = 2;
			$vendorGroup->write();
			
		}
	
		parent::requireDefaultRecords();
	}
	
	public function onBeforeWrite() {
		parent::onBeforeWrite();
		
		// Only set code property when the group has a custom title, and no code exists.
		// The "Code" attribute is usually treated as a more permanent identifier than database IDs
		// in custom application logic, so can't be changed after its first set.
		if(!$this->Code && $this->Title != _t('SecurityAdmin.NEWGROUP',"New Group")) {
			if(!$this->Code) Group::setCode($this->Title);
		}
	}
	
}